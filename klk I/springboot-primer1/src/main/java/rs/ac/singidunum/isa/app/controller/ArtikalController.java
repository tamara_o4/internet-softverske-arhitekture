package rs.ac.singidunum.isa.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.model.Artikal;
import rs.ac.singidunum.isa.app.service.ArtikalService;

// vrsi obradu, obradi zahtev i vrati rezultat
// http zahtevi get post put delete

@Controller
@RequestMapping(path = "/api/artikli")
public class ArtikalController {
	@Autowired
	private ArtikalService artikalService; 								
	
	// moramo mapirati metodu
	// PRIKAZ SVIH ARTIKALA
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<List<Artikal>> getAllArtikli(@RequestParam(name = "min", required = false) Double min, @RequestParam(name = "max", required = false) Double max) {
		//potrebano zbog pretrage cene izmeduju
		if (min == null) {
			min = -Double.MIN_VALUE;
		}
		if (max == null) {
			max = Double.MAX_VALUE;
		}
		
		System.out.println(min);
		System.out.println(min);
		return new ResponseEntity<List<Artikal>>(artikalService.findByPriceBetween(min, max), HttpStatus.OK);	// mora da se uveze da ne bi bio null pointer u Artikli service
	}
	
	// moramo mapirati metodu
	// PRIKAZ JEDNOG ARTIKLA
	@RequestMapping(path = "/{artikalId}", method = RequestMethod.GET)
	public ResponseEntity<Artikal> getArtikal(@PathVariable("artikalId") String artikalId) {
		//provera
		Artikal artikal = artikalService.findOne(artikalId);
		if(artikal != null) {
			return new ResponseEntity<Artikal>(artikal, HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);	// mora da se uveze da ne bi bio null pointer u Artikli service
	}
	
	// DODAVANJE
	@RequestMapping(path= "", method = RequestMethod.POST)
	public ResponseEntity<Artikal> createArtikal(@RequestBody Artikal artikal){
		if(artikalService.save(artikal)) {
			return new ResponseEntity<Artikal>(artikal, HttpStatus.CREATED);
		}
		return new ResponseEntity<Artikal>(HttpStatus.BAD_REQUEST);
	}
	
	// IZMENA
	@RequestMapping(path = "/{artikalId}", method = RequestMethod.PUT)
	public ResponseEntity<Artikal> updateArtikal(@PathVariable("artikalId") String arikalId, @RequestBody Artikal izmenjenArtikal) {
		if(artikalService.findOne(arikalId) != null) {
			izmenjenArtikal.setId(arikalId);
			artikalService.update(izmenjenArtikal);
			return new ResponseEntity<Artikal>(izmenjenArtikal, HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
		
	}
	
	// BRISANJE
	@RequestMapping(path = "/{artikalId}", method = RequestMethod.DELETE)
	public ResponseEntity<Artikal> deleteArtikal(@PathVariable("artikalId") String artikalId) {
		if(artikalService.findOne(artikalId) != null) {
			artikalService.delete(artikalId);
			return new ResponseEntity<Artikal>(HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
	}
	
	// 
	
	// metoda za pronalazenje artikala izmedju nekih iznosa implementirana je u findAll 
		
	
}
