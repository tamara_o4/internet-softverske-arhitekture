package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.dto.ApstraktDTO;
import rs.ac.singidunum.isa.app.dto.AutoriDTO;
import rs.ac.singidunum.isa.app.dto.KategorijaDTO;
import rs.ac.singidunum.isa.app.dto.KljucneReciDTO;
import rs.ac.singidunum.isa.app.dto.NaucniRadDTO;
import rs.ac.singidunum.isa.app.model.Autori;
import rs.ac.singidunum.isa.app.model.NaucniRad;
import rs.ac.singidunum.isa.app.service.NaucniRadService;

@Controller
@RequestMapping(path = "/api/radovi")
public class NaucniRadController {
	@Autowired
	NaucniRadService naucniRadService;
	
//	@RequestMapping(path = "", method = RequestMethod.GET)
//	public ResponseEntity<Iterable<NaucniRadDTO>> getAllnaucniRadovi() {
//		ArrayList<NaucniRadDTO> naucniRadovi = new ArrayList<NaucniRadDTO>();
//		// autor
//		// kljucne reci
//		// apstrakti
//		// kategorije
//		
//		for(NaucniRad naucniRad : naucniRadService.findAll()) {
//			//ApstraktDTO apstrakt = new ApstraktDTO(naucniRad.getApstrakt().getId(), naucniRad.getApstrakt().getTekst(), null);
//			KategorijaDTO kategoija = new KategorijaDTO(naucniRad.getKategorija().getId(), naucniRad.getKategorija().getNaziv(), null);
//			ArrayList<AutoriDTO> autori = new ArrayList<AutoriDTO>();
//			for(Autori autor : naucniRad.getAutori()) {
//				// treba dodati neregistrovanog korisnka, nastavnika, studenta
//				//autori.add(new AutoriDTO(autor.getId(), autor.getNeregistrovaniKorisnik(), autor.getNastavnik(), autor.getStudent()));
//			}
//			naucniRadovi.add(new NaucniRadDTO(naucniRad.getId(), naucniRad.getNaziv()));
//		}
//		
//		return new ResponseEntity<Iterable<NaucniRadDTO>>(naucniRadovi, HttpStatus.OK);	
//	}
	
	// pretraga po nazivu
	@RequestMapping(path = "/naziv", method = RequestMethod.GET)
	public ResponseEntity<Iterable<NaucniRadDTO>> getAllnaucniRadoviNaziv(@RequestParam(name = "naziv", required = false) String naziv) {
		ArrayList<NaucniRadDTO> naucniRadovi = new ArrayList<NaucniRadDTO>();
		if(naziv ==  null) {
			for(NaucniRad naucniRad : naucniRadService.findAll()) {
				naucniRadovi.add(new NaucniRadDTO(naucniRad.getId(), naucniRad.getNaziv()));
			}
		} else {
			for(NaucniRad naucniRad : naucniRadService.findByNazivStartingWith(naziv)) {
				naucniRadovi.add(new NaucniRadDTO(naucniRad.getId(), naucniRad.getNaziv()));
			}
		}
		return new ResponseEntity<Iterable<NaucniRadDTO>>(naucniRadovi, HttpStatus.OK);	
	}
	
//	@RequestMapping(path = "/reci", method = RequestMethod.GET)
//	public ResponseEntity<Iterable<NaucniRadDTO>> getAllnaucniRadoviReci(@RequestParam(name = "reci", required = false) String reci) {
//		ArrayList<NaucniRadDTO> naucniRadovi = new ArrayList<NaucniRadDTO>();
//		if(reci ==  null) {
//			for(NaucniRad naucniRad : naucniRadService.findAll()) {
//				naucniRadovi.add(new NaucniRadDTO(naucniRad.getId(), naucniRad.getNaziv()));
//			}
//		} else {
//			for(NaucniRad naucniRad : naucniRadService.findAll()) {
//				ArrayList<KljucneReciDTO> kljucneReci = new ArrayList<KljucneReciDTO>();
//				// koliko sam ja shvatila za vezu vise treba join da bi se uvezalo
////				for (KljucneReci)
////				naucniRadovi.add(new NaucniRadDTO(naucniRad.getId(), naucniRad.getNaziv()));
//			}
//		}
//		return new ResponseEntity<Iterable<NaucniRadDTO>>(naucniRadovi, HttpStatus.OK);	
//	}
	

	
	
	
}
