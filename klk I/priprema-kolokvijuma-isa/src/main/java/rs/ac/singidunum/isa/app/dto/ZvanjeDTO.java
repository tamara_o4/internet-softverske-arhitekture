package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.NaucnaOblast;
import rs.ac.singidunum.isa.app.model.TipZvanja;

public class ZvanjeDTO {
	private Long id;
	private Date datumIzbora;
	private Date datumPrestanka;
	
	private Nastavnik nastavnik;
	
	private Set<NaucnaOblast> naucneOblasti = new HashSet<NaucnaOblast>();	


	private Set<TipZvanja> tipZvanja = new HashSet<TipZvanja>();

	public ZvanjeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}



	public ZvanjeDTO(Long id, Date datumIzbora, Date datumPrestanka, Nastavnik nastavnik,
			Set<NaucnaOblast> naucneOblasti, Set<TipZvanja> tipZvanja) {
		super();
		this.id = id;
		this.datumIzbora = datumIzbora;
		this.datumPrestanka = datumPrestanka;
		this.nastavnik = nastavnik;
		this.naucneOblasti = naucneOblasti;
		this.tipZvanja = tipZvanja;
	}



	public ZvanjeDTO(Long id, Date datumIzbora, Date datumPrestanka, Nastavnik nastavnik ) {
		super();
		this.id = id;
		this.datumIzbora = datumIzbora;
		this.datumPrestanka = datumPrestanka;
		this.nastavnik = nastavnik;
		new ArrayList<NaucnaOblast>();
		new ArrayList<TipZvanja>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatumIzbora() {
		return datumIzbora;
	}

	public void setDatumIzbora(Date datumIzbora) {
		this.datumIzbora = datumIzbora;
	}

	public Date getDatumPrestanka() {
		return datumPrestanka;
	}

	public void setDatumPrestanka(Date datumPrestanka) {
		this.datumPrestanka = datumPrestanka;
	}

	public Nastavnik getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}

	public Set<NaucnaOblast> getNaucneOblasti() {
		return naucneOblasti;
	}

	public void setNaucneOblasti(Set<NaucnaOblast> naucneOblasti) {
		this.naucneOblasti = naucneOblasti;
	}

	public Set<TipZvanja> getTipZvanja() {
		return tipZvanja;
	}

	public void setTipZvanja(Set<TipZvanja> tipZvanja) {
		this.tipZvanja = tipZvanja;
	}

	

	
}
