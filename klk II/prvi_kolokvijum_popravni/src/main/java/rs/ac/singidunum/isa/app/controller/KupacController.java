package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.dto.KartaDTO;
import rs.ac.singidunum.isa.app.dto.KupacDTO;
import rs.ac.singidunum.isa.app.dto.ProjekcijaDTO;
import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.model.Kupac;
import rs.ac.singidunum.isa.app.service.KupacSerivice;

@Controller
@RequestMapping(path = "/api/kupci")
public class KupacController {
	@Autowired
	private KupacSerivice kupacService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<KupacDTO>> getAllKupac(@RequestParam(name = "id", required = false) Long id){
		ArrayList<KupacDTO> kupci = new ArrayList<KupacDTO>();
		if (id == null) {
			for(Kupac kupac : kupacService.findAll()) {
				ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
				for (Karta karta : kupac.getKarte()) {
					ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena() );
					karte.add(new KartaDTO(karta.getId(), karta.getPocetakPrijave(), karta.getBrojSedista(), karta.getCena(), null, p ));
				}
				kupci.add(new KupacDTO(kupac.getId(), kupac.getIme(), kupac.getPrezime(), karte));
			}
		}
		else {
			ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
			for(Karta karta : kupacService.findByProjekcijaId(id)) {
				ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena());
				karte.add(new KartaDTO(karta.getId(), karta.getPocetakPrijave(), karta.getBrojSedista(), karta.getCena(), null, p));
				KupacDTO kupac = new KupacDTO(karta.getKupac().getId(), karta.getKupac().getIme(), karta.getKupac().getPrezime());
				kupci.add(new KupacDTO(kupac.getId(), kupac.getIme(), kupac.getPrezime(), karte));
			}
		}
		return new ResponseEntity<Iterable<KupacDTO>>(kupci, HttpStatus.OK);
	}
	
	// pretraga po idju
		@RequestMapping(path = "/{kupacId}", method = RequestMethod.GET)
		public ResponseEntity<KupacDTO> getKupac(@PathVariable("kupacId") Long kupacId) {
			Optional<Kupac> kupac = kupacService.findOne(kupacId);
			KupacDTO kupacDTO;
			if (kupac.isPresent()) {
				ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
				for (Karta karta : kupac.get().getKarte()) {
					ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena());
					karte.add(new KartaDTO(karta.getId(), karta.getPocetakPrijave(), karta.getBrojSedista(), karta.getCena(), null, p));
				}
				kupacDTO = new KupacDTO(kupac.get().getId(), kupac.get().getIme(), kupac.get().getPrezime(), karte);
				return new ResponseEntity<KupacDTO>(kupacDTO, HttpStatus.OK);
			}
			return new ResponseEntity<KupacDTO>(HttpStatus.NOT_FOUND);
		}
		
		// novi
		@RequestMapping(path = "", method = RequestMethod.POST)
		public ResponseEntity<Kupac> createKupac(@RequestBody Kupac kupac) {
			try {
				kupac.setId(null);
				kupacService.save(kupac);
				return new ResponseEntity<Kupac>(kupac, HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new ResponseEntity<Kupac>(HttpStatus.BAD_REQUEST);
		}
		
		// izmena
		@RequestMapping(path = "/{kupacId}", method = RequestMethod.PUT)
		public ResponseEntity<KupacDTO> updateKarta(@PathVariable("kupacId") Long kupacId,
				@RequestBody Kupac izmenjeniKupac) {
			Kupac kupac = kupacService.findOne(kupacId).orElse(null);
			if (kupac != null) {
				izmenjeniKupac.setId(kupacId);
				izmenjeniKupac = kupacService.save(izmenjeniKupac);
//				PutnikDTO putnik = new PutnikDTO(izmenjeniKarta.getPutnik().getId(), izmenjeniKarta.getPutnik().getIme(), izmenjeniKarta.getPutnik().getPrezime());
//				StanicaDTO stanica = new StanicaDTO(izmenjeniKarta.getStanica().getId(), izmenjeniKarta.getStanica().getNaziv(), izmenjeniKarta.getStanica().getAdresa());
				
				//KartaDTO izmenjenaKartaDto = new KartaDTO(izmenjeniKarta.getId(), izmenjeniKarta.getDatumVremePolaska(), izmenjeniKarta.isPrtljag(),izmenjeniKarta.getCena(), putnik, stanica); 
				ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
				for (Karta karta : izmenjeniKupac.getKarte()) {
					ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena());
					karte.add(new KartaDTO(karta.getId(), karta.getPocetakPrijave(), karta.getBrojSedista(), karta.getCena(), null, p));
				}
				KupacDTO izmenjeniKupacDto = new KupacDTO(izmenjeniKupac.getId(), izmenjeniKupac.getIme(), izmenjeniKupac.getPrezime(), karte);
				
				return new ResponseEntity<KupacDTO>(izmenjeniKupacDto, HttpStatus.OK);
			}
			return new ResponseEntity<KupacDTO>(HttpStatus.NOT_FOUND);
		}
		
		// brisanje
		@RequestMapping(path = "/{kupacId}", method = RequestMethod.DELETE)
		public ResponseEntity<Kupac> deleteKarta(@PathVariable("kupacId") Long kupacId) {
			if (kupacService.findOne(kupacId).isPresent()) {
				kupacService.delete(kupacId);
				return new ResponseEntity<Kupac>(HttpStatus.OK);
			}
			return new ResponseEntity<Kupac>(HttpStatus.NOT_FOUND);
		}
}
