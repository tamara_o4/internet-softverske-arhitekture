package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class StudentDTO {
private Long id;
	
	private String indeks;
	private String ime;
	private String prezime;
	private String email;
	private String lozinka;
	
	private FinansijskaKarticaDTO finansijskaKartica;

	public StudentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudentDTO(Long id, String indeks, String ime, String prezime, String email, String lozinka,
			FinansijskaKarticaDTO finansijskaKartica) {
		super();
		this.id = id;
		this.indeks = indeks;
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.lozinka = lozinka;
		this.finansijskaKartica = finansijskaKartica;
	}

	public StudentDTO(Long id, String indeks, String ime, String prezime, String email, String lozinka) {
		super();
		this.id = id;
		this.indeks = indeks;
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.lozinka = lozinka;
	}

	
	

	public StudentDTO(Long id, String indeks, String lozinka) {
		super();
		this.id = id;
		this.indeks = indeks;
		this.lozinka = lozinka;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndeks() {
		return indeks;
	}

	public void setIndeks(String indeks) {
		this.indeks = indeks;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public FinansijskaKarticaDTO getFinansijskaKartica() {
		return finansijskaKartica;
	}

	public void setFinansijskaKartica(FinansijskaKarticaDTO finansijskaKartica) {
		this.finansijskaKartica = finansijskaKartica;
	}

	
	
}
