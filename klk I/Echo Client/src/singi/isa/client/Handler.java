package singi.isa.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Handler implements Runnable {

	@Override
	public void run() {
		try {
			Socket client = new Socket("localhost", 3000);
			
			PrintWriter os = new PrintWriter(client.getOutputStream());
			BufferedReader is = new BufferedReader(new InputStreamReader(client.getInputStream()));
			
			os.println("Zahtev!");
			os.flush();
			
			System.out.println(is.readLine());
			os.close();
			is.close();
			client.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
