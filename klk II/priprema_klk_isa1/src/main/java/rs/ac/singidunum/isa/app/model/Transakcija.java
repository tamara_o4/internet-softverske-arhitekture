package rs.ac.singidunum.isa.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class Transakcija {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	private Double iznos;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datumValute;

	@ManyToOne(optional = false)
	private FinansijskaKartica finansijskaKartica;

	public Transakcija(Long id, Double iznos, Date datumValute, FinansijskaKartica finansijskaKartica) {
		super();
		this.id = id;
		this.iznos = iznos;
		this.datumValute = datumValute;
		this.finansijskaKartica = finansijskaKartica;
	}

	public Transakcija() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Transakcija(Long id, Double iznos, Date datumValute) {
		super();
		this.id = id;
		this.iznos = iznos;
		this.datumValute = datumValute;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getIznos() {
		return iznos;
	}

	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}

	public Date getDatumValute() {
		return datumValute;
	}

	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}
	
	public FinansijskaKartica getFinansijskaKartica() {
		return finansijskaKartica;
	}

	public void setFinansijskaKartica(FinansijskaKartica finansijskaKartica) {
		this.finansijskaKartica = finansijskaKartica;
	}
	
}
