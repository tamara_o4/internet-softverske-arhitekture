package rs.ac.singidunum.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Automobil;
import rs.ac.singidunum.isa.app.repository.AutomobilRepository;

@Service
public class AutomobilService {
	@Autowired
	private AutomobilRepository automobilRepository;

	public AutomobilService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AutomobilService(AutomobilRepository automobilRepository) {
		super();
		this.automobilRepository = automobilRepository;
	}

	public AutomobilRepository getAutomobilRepository() {
		return automobilRepository;
	}

	public void setAutomobilRepository(AutomobilRepository automobilRepository) {
		this.automobilRepository = automobilRepository;
	}
	
	public Iterable<Automobil> findAll(){
		return automobilRepository.findAll();
	}
	
	public Optional<Automobil> findOne(Long id) {
		return automobilRepository.findById(id);
	}
	
	public Automobil save(Automobil automobil){
		return automobilRepository.save(automobil);
	}
		
	public void delete(Long id) {
		 automobilRepository.deleteById(id);
	}
	
	public void delete(Automobil automobil) {
		 automobilRepository.delete(automobil);
	}
	
	
}
