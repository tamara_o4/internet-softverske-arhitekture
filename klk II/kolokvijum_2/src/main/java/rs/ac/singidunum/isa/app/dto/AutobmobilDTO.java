package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class AutobmobilDTO {
private Long id;
	
	private String naziv;
	private double cena;
	private long kolicina;
	private ArrayList<RegistracijaDTO> registracije = new ArrayList<RegistracijaDTO>();
	public AutobmobilDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public AutobmobilDTO(Long id, String naziv, double cena, long kolicina, ArrayList<RegistracijaDTO> registracije) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.cena = cena;
		this.kolicina = kolicina;
		this.registracije = registracije;
	}
	
	public AutobmobilDTO(Long id, String naziv, double cena, long kolicina) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.cena = cena;
		this.kolicina = kolicina;
		new ArrayList<RegistracijaDTO>();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public long getKolicina() {
		return kolicina;
	}
	public void setKolicina(long kolicina) {
		this.kolicina = kolicina;
	}
	public ArrayList<RegistracijaDTO> getKarte() {
		return registracije;
	}
	public void setKarte(ArrayList<RegistracijaDTO> registracije) {
		this.registracije = registracije;
	}
	
	

}
