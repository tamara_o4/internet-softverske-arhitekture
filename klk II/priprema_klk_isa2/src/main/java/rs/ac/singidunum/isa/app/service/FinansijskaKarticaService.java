package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.FinansijskaKartica;
import rs.ac.singidunum.isa.app.repository.FinansijskaKarticaRepository;

@Service
public class FinansijskaKarticaService {
	@Autowired
	private FinansijskaKarticaRepository finansijskaKarticaRepository;
	
	public Iterable<FinansijskaKartica> findAll(){
		return finansijskaKarticaRepository.findAll();
	}
	
	public Optional<FinansijskaKartica> findOne(Long id) {
		return finansijskaKarticaRepository.findById(id);
	}
	
	public FinansijskaKartica save(FinansijskaKartica korisnik){
		return finansijskaKarticaRepository.save(korisnik);
	}
		
	public void delete(Long id) {
		 finansijskaKarticaRepository.deleteById(id);
	}
	
	public void delete(FinansijskaKartica korisnik) {
		 finansijskaKarticaRepository.delete(korisnik);
	}
}
