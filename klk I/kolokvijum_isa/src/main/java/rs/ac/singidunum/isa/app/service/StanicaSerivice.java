package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Stanica;
import rs.ac.singidunum.isa.app.repository.StanicaRepository;

@Service
public class StanicaSerivice {
	@Autowired
	private StanicaRepository stanicaRepository;

	public StanicaSerivice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StanicaSerivice(StanicaRepository stanicaRepository) {
		super();
		this.stanicaRepository = stanicaRepository;
	}

	public StanicaRepository getStanicaRepository() {
		return stanicaRepository;
	}

	public void setStanicaRepository(StanicaRepository stanicaRepository) {
		this.stanicaRepository = stanicaRepository;
	}
	
	public Iterable<Stanica> findAll(){
		return stanicaRepository.findAll();
	}
	
	public Optional<Stanica> findOne(Long id) {
		return stanicaRepository.findById(id);
	}
	
	//
//	public List<Artikal> findByPriceBetween(double min, double max){
//		return artikalRepository.findByPriceBetween(min, max);
//	}
	
	public Stanica save(Stanica Stanica){
		return stanicaRepository.save(Stanica);
	}
		
	public void delete(Long id) {
		stanicaRepository.deleteById(id);
	}
	
	public void delete(Stanica Stanica) {
		stanicaRepository.delete(Stanica);
	}
	
	
}
