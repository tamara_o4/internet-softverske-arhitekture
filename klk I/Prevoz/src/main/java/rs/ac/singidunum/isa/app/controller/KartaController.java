package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.dto.KartaDTO;
import rs.ac.singidunum.isa.app.dto.PutnikDTO;
import rs.ac.singidunum.isa.app.dto.StanicaDTO;
import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.service.KartaService;

@Controller
@RequestMapping(path = "/api/karte")
public class KartaController {
	@Autowired
	private KartaService kartaService;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<KartaDTO>> getAllKarte(@RequestParam(name = "id", required = false) Long id){
		ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
		if (id == null) {
			for(Karta karta : kartaService.findAll()) {
				PutnikDTO putnik = new PutnikDTO(karta.getPutnik().getId(), karta.getPutnik().getIme(), karta.getPutnik().getPrezime());
				StanicaDTO stanica = new StanicaDTO(karta.getStanica().getId(), karta.getStanica().getNaziv(), karta.getStanica().getAdresa());
				karte.add(new KartaDTO(karta.getId(), karta.getDatumVremePolaska(), karta.isPrtljag(), karta.getCena(), putnik, stanica));
			}
		}
		else {
			for(Karta karta : kartaService.findByPutnikId(id)) {
				PutnikDTO putnik = new PutnikDTO(karta.getPutnik().getId(), karta.getPutnik().getIme(), karta.getPutnik().getPrezime());
				StanicaDTO stanica = new StanicaDTO(karta.getStanica().getId(), karta.getStanica().getNaziv(), karta.getStanica().getAdresa());
				karte.add(new KartaDTO(karta.getId(), karta.getDatumVremePolaska(), karta.isPrtljag(), karta.getCena(), putnik, stanica));
			}
		}
		return new ResponseEntity<Iterable<KartaDTO>>(karte, HttpStatus.OK);
	}
	
	
	// pretraga po idju
	@RequestMapping(path = "/{kartaId}", method = RequestMethod.GET)
	public ResponseEntity<KartaDTO> getKarta(@PathVariable("kartaId") Long kartaId) {
		Optional<Karta> karta = kartaService.findOne(kartaId);
		KartaDTO kartaDTO;
		if (karta.isPresent()) {
			PutnikDTO putnik = new PutnikDTO(karta.get().getPutnik().getId(), karta.get().getPutnik().getIme(), karta.get().getPutnik().getPrezime());
			StanicaDTO stanica = new StanicaDTO(karta.get().getStanica().getId(), karta.get().getStanica().getNaziv(), karta.get().getStanica().getAdresa());
			kartaDTO = new KartaDTO(karta.get().getId(), karta.get().getDatumVremePolaska(), karta.get().isPrtljag(), karta.get().getCena(), putnik, stanica);
			
			return new ResponseEntity<KartaDTO>(kartaDTO, HttpStatus.OK);
		}
		return new ResponseEntity<KartaDTO>(HttpStatus.NOT_FOUND);
	}
	
	// novi
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Karta> createKarta(@RequestBody Karta karta) {
		try {
			karta.setId(null);
			kartaService.save(karta);
			return new ResponseEntity<Karta>(karta, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Karta>(HttpStatus.BAD_REQUEST);
	}
	
	// izmena
	@RequestMapping(path = "/{kartaId}", method = RequestMethod.PUT)
	public ResponseEntity<KartaDTO> updateKarta(@PathVariable("kartaId") Long kartaId,
			@RequestBody Karta izmenjeniKarta) {
		Karta karta = kartaService.findOne(kartaId).orElse(null);
		if (karta != null) {
			izmenjeniKarta.setId(kartaId);
			izmenjeniKarta = kartaService.save(izmenjeniKarta);
			PutnikDTO putnik = new PutnikDTO(izmenjeniKarta.getPutnik().getId(), izmenjeniKarta.getPutnik().getIme(), izmenjeniKarta.getPutnik().getPrezime());
			StanicaDTO stanica = new StanicaDTO(izmenjeniKarta.getStanica().getId(), izmenjeniKarta.getStanica().getNaziv(), izmenjeniKarta.getStanica().getAdresa());
			KartaDTO izmenjenaKartaDto = new KartaDTO(izmenjeniKarta.getId(), izmenjeniKarta.getDatumVremePolaska(), izmenjeniKarta.isPrtljag(),izmenjeniKarta.getCena(), putnik, stanica); 
			return new ResponseEntity<KartaDTO>(izmenjenaKartaDto, HttpStatus.OK);
		}
		return new ResponseEntity<KartaDTO>(HttpStatus.NOT_FOUND);
	}
	
	// brisanje
	@RequestMapping(path = "/{kartaId}", method = RequestMethod.DELETE)
	public ResponseEntity<Karta> deleteKarta(@PathVariable("kartaId") Long kartaId) {
		if (kartaService.findOne(kartaId).isPresent()) {
			kartaService.delete(kartaId);
			return new ResponseEntity<Karta>(HttpStatus.OK);
		}
		return new ResponseEntity<Karta>(HttpStatus.NOT_FOUND);
	}
}
