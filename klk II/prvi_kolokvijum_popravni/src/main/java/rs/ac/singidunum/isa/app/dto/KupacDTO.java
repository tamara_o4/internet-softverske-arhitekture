package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;


public class KupacDTO {
	private Long id;
	
	private String ime;
	private String prezime;
	private ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
	public KupacDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public KupacDTO(Long id, String ime, String prezime, ArrayList<KartaDTO> karte) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.karte = karte;
	}
	
	public KupacDTO(Long id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		new ArrayList<KartaDTO>();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public ArrayList<KartaDTO> getKarte() {
		return karte;
	}
	public void setKarte(ArrayList<KartaDTO> karte) {
		this.karte = karte;
	}
	
	

}
