package rs.ac.singidunum.isa.app.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Popust {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;					// Long zbo identity, objekat je zato sto moze biti null
	
	@Lob
	private String naziv;
	@Temporal(TemporalType.TIMESTAMP)
	private Date pocetak;

	@Temporal(TemporalType.TIMESTAMP)
	private Date kraj;
	
	private double procenat;
	
	@OneToMany(mappedBy = "popust")
	private Set<ArtikalUMaloprodaji> artikliUmaloprodiji = new HashSet<ArtikalUMaloprodaji>();

	public Popust() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Popust(Long id, String naziv, Date pocetak, Date kraj, double procenat,
			Set<ArtikalUMaloprodaji> artikliUmaloprodiji) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.procenat = procenat;
		this.artikliUmaloprodiji = artikliUmaloprodiji;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Date getPocetak() {
		return pocetak;
	}

	public void setPocetak(Date pocetak) {
		this.pocetak = pocetak;
	}

	public Date getKraj() {
		return kraj;
	}

	public void setKraj(Date kraj) {
		this.kraj = kraj;
	}

	public double getProcenat() {
		return procenat;
	}

	public void setProcenat(double procenat) {
		this.procenat = procenat;
	}



	public Set<ArtikalUMaloprodaji> getArtikliUmaloprodiji() {
		return artikliUmaloprodiji;
	}



	public void setArtikliUmaloprodiji(Set<ArtikalUMaloprodaji> artikliUmaloprodiji) {
		this.artikliUmaloprodiji = artikliUmaloprodiji;
	}

	
	
	
}
