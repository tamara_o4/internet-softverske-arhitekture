package rs.ac.singidunum.isa.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class TipZvanja {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String naziv;
	
	@ManyToOne(optional = false)
	private Zvanje zvanje;


	public void setZvanje(Zvanje zvanje) {
		this.zvanje = zvanje;
	}

	public TipZvanja(Long id, String naziv, Zvanje zvanje) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.zvanje = zvanje;
	}

	public TipZvanja() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TipZvanja(Long id, String naziv) {
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	
//	@Column(nullable = false)
//	private String korisnickoIme;
//	@Column(nullable = false)
//	private String lozinka;
	
//	// moramo dodati zvog veze sa Kupovinom
//	@OneToMany(mappedBy = "korisnik")
//	private Set<Kupovina> kupovine = new HashSet<Kupovina>();
	
	
}
