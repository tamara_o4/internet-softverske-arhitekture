package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.StudentDTO;
import rs.ac.singidunum.isa.app.model.Student;
import rs.ac.singidunum.isa.app.service.StudentService;

@Controller
@RequestMapping(path = "/api/studenti")
public class StudentController {
	@Autowired
	private StudentService studentService;
	
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<StudentDTO>> getAllKarte(){
		ArrayList<StudentDTO> studenti = new ArrayList<StudentDTO>();
		for(Student student : studentService.findAll()) {
			//FinansijskaKarticaDTO kartica = new FinansijskaKarticaDTO(student.getFinansijskaKartica().getId(), student.getFinansijskaKartica().getPozivNaBroj(), student.getFinansijskaKartica().getStanje());
			studenti.add(new StudentDTO(student.getId(), student.getIndeks(), student.getIme(), student.getPrezime(), student.getEmail(), student.getLozinka(), null));
			
		}
		return new ResponseEntity<Iterable<StudentDTO>>(studenti, HttpStatus.OK);
	}
}
