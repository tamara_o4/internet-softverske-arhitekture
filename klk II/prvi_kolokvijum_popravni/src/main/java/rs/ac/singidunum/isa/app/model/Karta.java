package rs.ac.singidunum.isa.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
public class Karta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date pocetakPrijave;
	
	@Lob
	private String brojSedista;
	private Double cena;
	
	@ManyToOne(optional = false)
	private Kupac kupac;
	@ManyToOne(optional = false)
	private Projekcija projekcija;
	
	public Karta() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Karta(Long id, Date pocetakPrijave, String brojSedista, Double cena, Kupac kupac, Projekcija projekcija) {
		super();
		this.id = id;
		this.pocetakPrijave = pocetakPrijave;
		this.brojSedista = brojSedista;
		this.cena = cena;
		this.kupac = kupac;
		this.projekcija = projekcija;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getPocetakPrijave() {
		return pocetakPrijave;
	}
	public void setPocetakPrijave(Date pocetakPrijave) {
		this.pocetakPrijave = pocetakPrijave;
	}
	public String getBrojSedista() {
		return brojSedista;
	}
	public void setBrojSedista(String brojSedista) {
		this.brojSedista = brojSedista;
	}
	public Double getCena() {
		return cena;
	}
	public void setCena(Double cena) {
		this.cena = cena;
	}
	public Kupac getKupac() {
		return kupac;
	}
	public void setKupac(Kupac kupac) {
		this.kupac = kupac;
	}
	public Projekcija getProjekcija() {
		return projekcija;
	}
	public void setProjekcija(Projekcija projekcija) {
		this.projekcija = projekcija;
	}
	
	
}
