package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class FinansijskaKarticaDTO {
	private Long id;	
	private String pozivNaBroj;
	private Double stanje;
	private ArrayList<TransakcijaDTO> transakcije = new ArrayList<TransakcijaDTO>();
	
	private StudentDTO student;
	
	
	
	public FinansijskaKarticaDTO(Long id, String pozivNaBroj, Double stanje, ArrayList<TransakcijaDTO> transakcije,
			StudentDTO student) {
		super();
		this.id = id;
		this.pozivNaBroj = pozivNaBroj;
		this.stanje = stanje;
		this.transakcije = transakcije;
		this.student = student;
	
	}
	
	public FinansijskaKarticaDTO(Long id, String pozivNaBroj, Double stanje) {
		super();
		this.id = id;
		this.pozivNaBroj = pozivNaBroj;
		this.stanje = stanje;
		new ArrayList<TransakcijaDTO>();
	
	}

	
	public StudentDTO getStudent() {
		return student;
	}
	public void setStudent(StudentDTO student) {
		this.student = student;
	}
	public FinansijskaKarticaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public FinansijskaKarticaDTO(Long id, String pozivNaBroj, Double stanje, ArrayList<TransakcijaDTO> transakcije) {
		super();
		this.id = id;
		this.pozivNaBroj = pozivNaBroj;
		this.stanje = stanje;
		this.transakcije = transakcije;
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPozivNaBroj() {
		return pozivNaBroj;
	}
	public void setPozivNaBroj(String pozivNaBroj) {
		this.pozivNaBroj = pozivNaBroj;
	}
	public Double getStanje() {
		return stanje;
	}
	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}
	public ArrayList<TransakcijaDTO> getTransakcije() {
		return transakcije;
	}
	public void setTransakcije(ArrayList<TransakcijaDTO> transakcije) {
		this.transakcije = transakcije;
	}

	
	
	
	
}
