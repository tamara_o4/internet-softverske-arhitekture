package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;


@Entity
public class Projekcija {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String film;
	@Lob
	private String zanr;
	private double ocena;
	
	@OneToMany(mappedBy = "projekcija")
	private Set<Karta> karte = new HashSet<Karta>();

	public Projekcija() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Projekcija(Long id, String film, String zanr, double ocena, Set<Karta> karte) {
		super();
		this.id = id;
		this.film = film;
		this.zanr = zanr;
		this.ocena = ocena;
		this.karte = karte;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFilm() {
		return film;
	}

	public void setFilm(String film) {
		this.film = film;
	}

	public String getZanr() {
		return zanr;
	}

	public void setZanr(String zanr) {
		this.zanr = zanr;
	}

	public double getOcena() {
		return ocena;
	}

	public void setOcena(double ocena) {
		this.ocena = ocena;
	}

	public Set<Karta> getKarte() {
		return karte;
	}

	public void setKarte(Set<Karta> karte) {
		this.karte = karte;
	}

	
}
