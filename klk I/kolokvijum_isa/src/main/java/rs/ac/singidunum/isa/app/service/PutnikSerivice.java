package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Putnik;
import rs.ac.singidunum.isa.app.repository.PutnikRepository;

@Service
public class PutnikSerivice {
	@Autowired
	private PutnikRepository putnikRepository;

	public PutnikSerivice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PutnikSerivice(PutnikRepository putnikRepository) {
		super();
		this.putnikRepository = putnikRepository;
	}

	public PutnikRepository getPutnikRepository() {
		return putnikRepository;
	}

	public void setPutnikRepository(PutnikRepository putnikRepository) {
		this.putnikRepository = putnikRepository;
	}
	
	public Iterable<Putnik> findAll(){
		return putnikRepository.findAll();
	}
	
	public Optional<Putnik> findOne(Long id) {
		return putnikRepository.findById(id);
	}
	
	//
//	public List<Artikal> findByPriceBetween(double min, double max){
//		return artikalRepository.findByPriceBetween(min, max);
//	}
	
	public Putnik save(Putnik Putnik){
		return putnikRepository.save(Putnik);
	}
		
	public void delete(Long id) {
		putnikRepository.deleteById(id);
	}
	
	public void delete(Putnik Putnik) {
		putnikRepository.delete(Putnik);
	}
	
	
}
