package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.ArtikalUMaloprodajiDTO;
import rs.ac.singidunum.isa.app.dto.PopustDTO;
import rs.ac.singidunum.isa.app.model.ArtikalUMaloprodaji;
import rs.ac.singidunum.isa.app.model.Popust;
import rs.ac.singidunum.isa.app.service.PopustService;

@Controller
@RequestMapping(path = "/api/popusti")
public class PopustController {
	@Autowired PopustService popustSevice;

	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<PopustDTO>> getAllPopusti() {
		ArrayList<PopustDTO> popusti = new ArrayList<PopustDTO>();
		for (Popust popust : popustSevice.findAll()) {
			ArrayList<ArtikalUMaloprodajiDTO> artikliUMaloprodaji = new ArrayList<ArtikalUMaloprodajiDTO>();
			for(ArtikalUMaloprodaji kupovina : popust.getArtikliUmaloprodiji()) {
				artikliUMaloprodaji.add(new ArtikalUMaloprodajiDTO(kupovina.getId(), kupovina.getCena(), kupovina.getKolicina(), null));
			}
			popusti.add(new PopustDTO(popust.getId(), popust.getNaziv(), popust.getPocetak(), popust.getKraj(), popust.getProcenat(), artikliUMaloprodaji));
			
		}
		return new ResponseEntity<Iterable<PopustDTO>>(popusti, HttpStatus.OK);
	}
	

}
