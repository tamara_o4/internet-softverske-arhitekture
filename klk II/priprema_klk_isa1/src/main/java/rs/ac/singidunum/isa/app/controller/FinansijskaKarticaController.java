package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.FinansijskaKarticaDTO;
import rs.ac.singidunum.isa.app.model.FinansijskaKartica;
import rs.ac.singidunum.isa.app.model.Student;
import rs.ac.singidunum.isa.app.service.FinansijskaKarticaService;

@Controller
@RequestMapping(path = "/api/finansijskaKartica")
public class FinansijskaKarticaController {
	@Autowired
	private FinansijskaKarticaService finansijskaKarticaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<FinansijskaKarticaDTO>> getAllFinansijskeKartice(){
		ArrayList<FinansijskaKarticaDTO> kartice = new ArrayList<FinansijskaKarticaDTO>();
		for(FinansijskaKartica kartica : finansijskaKarticaService.findAll()) {
			//FinansijskaKarticaDTO kartica = new FinansijskaKarticaDTO(student.getFinansijskaKartica().getId(), student.getFinansijskaKartica().getPozivNaBroj(), student.getFinansijskaKartica().getStanje());
			//studenti.add(new FinansijskaKarticaDTO(student.getId(), student.getIndeks(), student.getIme(), student.getPrezime(), student.getEmail(), student.getLozinka(), null));
			kartice.add(new FinansijskaKarticaDTO(kartica.getId(), kartica.getPozivNaBroj(), kartica.getStanje()));
		}
		return new ResponseEntity<Iterable<FinansijskaKarticaDTO>>(kartice, HttpStatus.OK);
	}
	
}
