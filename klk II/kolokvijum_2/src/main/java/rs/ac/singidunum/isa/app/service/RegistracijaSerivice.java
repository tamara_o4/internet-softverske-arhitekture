package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Registracija;
import rs.ac.singidunum.isa.app.repository.RegistracijaRepository;

@Service
public class RegistracijaSerivice {
	@Autowired
	private RegistracijaRepository registracijaRepository;

	public RegistracijaSerivice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegistracijaSerivice(RegistracijaRepository registracijaRepository) {
		super();
		this.registracijaRepository = registracijaRepository;
	}

	public RegistracijaRepository getRegistracijaRepository() {
		return registracijaRepository;
	}

	public void setRegistracijaRepository(RegistracijaRepository registracijaRepository) {
		this.registracijaRepository = registracijaRepository;
	}
	
	public Iterable<Registracija> findAll(){
		return registracijaRepository.findAll();
	}
	
	public Optional<Registracija> findOne(Long id) {
		return registracijaRepository.findById(id);
	}
	
	//
//	public List<Artikal> findByPriceBetween(double min, double max){
//		return artikalRepository.findByPriceBetween(min, max);
//	}
	
	public Registracija save(Registracija registracija){
		return registracijaRepository.save(registracija);
	}
		
	public void delete(Long id) {
		 registracijaRepository.deleteById(id);
	}
	
	public void delete(Registracija registracija) {
		 registracijaRepository.delete(registracija);
	}
	
	
}
