package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

@Entity
public class Stanica {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	@Lob
	private String naziv;
	@Lob
	private String adresa;
	
	@OneToMany(mappedBy = "stanica")
	private Set<Karta> karte = new HashSet<Karta>();

	public Stanica() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Stanica(Long id, String naziv, String adresa, Set<Karta> karte) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.adresa = adresa;
		this.karte = karte;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public Set<Karta> getKarte() {
		return karte;
	}

	public void setKarte(Set<Karta> karte) {
		this.karte = karte;
	}
	
	
}
