package rs.ac.singidunum.isa.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.ArtikalUMaloprodaji;

@Repository
public interface ArtikalUMaloprodajiRepository extends CrudRepository<ArtikalUMaloprodaji, Long> {

	@Query("SELECT popust FROM ArtikalUMaloprodaji popust WHERE id = :id")
	List<ArtikalUMaloprodaji> findByPopust(Long id);

}
