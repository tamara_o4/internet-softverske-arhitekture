package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.KartaDTO;
import rs.ac.singidunum.isa.app.dto.KupacDTO;
import rs.ac.singidunum.isa.app.dto.ProjekcijaDTO;
import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.model.Projekcija;
import rs.ac.singidunum.isa.app.service.ProjekcijaSerivice;

@Controller
@RequestMapping(path = "/api/projekciije")
public class ProjekcijaController {
	@Autowired
	private ProjekcijaSerivice projekcijaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<ProjekcijaDTO>> getAllProjekcija(){
		ArrayList<ProjekcijaDTO> projekcije = new ArrayList<ProjekcijaDTO>();
		for(Projekcija projekcija : projekcijaService.findAll()) {
			ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
			for (Karta karta : projekcija.getKarte()) {
				//ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena() );
				KupacDTO k = new KupacDTO(karta.getKupac().getId(), karta.getKupac().getIme(), karta.getKupac().getPrezime());
				karte.add(new KartaDTO(karta.getId(), karta.getPocetakPrijave(), karta.getBrojSedista(), karta.getCena(), k, null ));
			}
			projekcije.add(new ProjekcijaDTO(projekcija.getId(), projekcija.getFilm(), projekcija.getZanr(), projekcija.getOcena(), karte));
		}
		return new ResponseEntity<Iterable<ProjekcijaDTO>>(projekcije, HttpStatus.OK);
	}
	
	// pretraga po idju
	@RequestMapping(path = "/{projekcijaId}", method = RequestMethod.GET)
	public ResponseEntity<ProjekcijaDTO> getProjekcija(@PathVariable("projekcijaId") Long projekcijaId) {
		Optional<Projekcija> projekcija = projekcijaService.findOne(projekcijaId);
		ProjekcijaDTO projekcijaDTO;
		if (projekcija.isPresent()) {
			ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
			for (Karta karta : projekcija.get().getKarte()) {
				//ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena());
				KupacDTO k = new KupacDTO(karta.getKupac().getId(), karta.getKupac().getIme(), karta.getKupac().getPrezime());
				karte.add(new KartaDTO(karta.getId(), karta.getPocetakPrijave(), karta.getBrojSedista(), karta.getCena(), k, null));
			}
			projekcijaDTO = new ProjekcijaDTO(projekcija.get().getId(), projekcija.get().getFilm(), projekcija.get().getZanr(), projekcija.get().getOcena(), karte);
			return new ResponseEntity<ProjekcijaDTO>(projekcijaDTO, HttpStatus.OK);
		}
		return new ResponseEntity<ProjekcijaDTO>(HttpStatus.NOT_FOUND);
	}
		
		// novi
		@RequestMapping(path = "", method = RequestMethod.POST)
		public ResponseEntity<Projekcija> createProjekcija(@RequestBody Projekcija projekcija) {
			try {
				projekcija.setId(null);
				projekcijaService.save(projekcija);
				return new ResponseEntity<Projekcija>(projekcija, HttpStatus.CREATED);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new ResponseEntity<Projekcija>(HttpStatus.BAD_REQUEST);
		}
		
		// izmena
		@RequestMapping(path = "/{projekcijaId}", method = RequestMethod.PUT)
		public ResponseEntity<ProjekcijaDTO> updateKarta(@PathVariable("projekcijaId") Long projekcijaId,
				@RequestBody Projekcija izmenjeniProjekcija) {
			Projekcija projekcija = projekcijaService.findOne(projekcijaId).orElse(null);
			if (projekcija != null) {
				izmenjeniProjekcija.setId(projekcijaId);
				izmenjeniProjekcija = projekcijaService.save(izmenjeniProjekcija);
				ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
				for (Karta karta : izmenjeniProjekcija.getKarte()) {
					//ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena());
					KupacDTO k = new KupacDTO(karta.getKupac().getId(), karta.getKupac().getIme(), karta.getKupac().getPrezime());
					karte.add(new KartaDTO(karta.getId(), karta.getPocetakPrijave(), karta.getBrojSedista(), karta.getCena(), k, null));
				}
				ProjekcijaDTO izmenjeniProjekcijaDto = new ProjekcijaDTO(izmenjeniProjekcija.getId(), izmenjeniProjekcija.getFilm(), izmenjeniProjekcija.getZanr(), izmenjeniProjekcija.getOcena(), karte);
				
				return new ResponseEntity<ProjekcijaDTO>(izmenjeniProjekcijaDto, HttpStatus.OK);
			}
			return new ResponseEntity<ProjekcijaDTO>(HttpStatus.NOT_FOUND);
		}
		
		// brisanje
		@RequestMapping(path = "/{projekcijaId}", method = RequestMethod.DELETE)
		public ResponseEntity<Projekcija> deleteKarta(@PathVariable("projekcijaId") Long projekcijaId) {
			if (projekcijaService.findOne(projekcijaId).isPresent()) {
				projekcijaService.delete(projekcijaId);
				return new ResponseEntity<Projekcija>(HttpStatus.OK);
			}
			return new ResponseEntity<Projekcija>(HttpStatus.NOT_FOUND);
		}
}
