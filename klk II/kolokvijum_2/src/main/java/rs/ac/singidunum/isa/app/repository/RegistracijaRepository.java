package rs.ac.singidunum.isa.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Registracija;

@Repository
public interface RegistracijaRepository extends CrudRepository<Registracija, Long> {
	// pronalazi sve karte zadatog putnika
//	List<Registracija> findByStanicaId(Long id);
	
}
