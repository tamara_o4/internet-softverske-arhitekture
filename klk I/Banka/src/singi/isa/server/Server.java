package singi.isa.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	public static void main(String[] args) throws InterruptedException {
		int port = 3000;
		
		Racun racun = new Racun(1000);
		
		try (ServerSocket ss = new ServerSocket(port)){
			while(true) {
				
			
			Socket socket = ss.accept(); 				// uspostavljena veza, OVDE SE CITAJU ILI PISU PODACI
			Handler h = new Handler(socket, racun);
			Thread t = new Thread(h);
			t.start();
			
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.print("Doslo je do greske pri pokretanju servera!");
		}  	
		
	}
}
