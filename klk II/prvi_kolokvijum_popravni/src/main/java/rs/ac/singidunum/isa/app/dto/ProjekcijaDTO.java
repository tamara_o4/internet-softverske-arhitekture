package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;


public class ProjekcijaDTO {
	private Long id;
	
	private String film;
	private String zanr;
	private double ocena;
	
	
	private ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();


	public ProjekcijaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}




	public ProjekcijaDTO(Long id, String film, String zanr, double ocena, ArrayList<KartaDTO> karte) {
		super();
		this.id = id;
		this.film = film;
		this.zanr = zanr;
		this.ocena = ocena;
		this.karte = karte;
	}




	public ProjekcijaDTO(Long id, String film, String zanr, double ocena) {
		super();
		this.id = id;
		this.film = film;
		this.zanr = zanr;
		this.ocena = ocena;
		new ArrayList<KartaDTO>();
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getFilm() {
		return film;
	}


	public void setFilm(String film) {
		this.film = film;
	}


	public String getZanr() {
		return zanr;
	}


	public void setZanr(String zanr) {
		this.zanr = zanr;
	}


	public double getOcena() {
		return ocena;
	}


	public void setOcena(double ocena) {
		this.ocena = ocena;
	}




	public ArrayList<KartaDTO> getKarte() {
		return karte;
	}




	public void setKarte(ArrayList<KartaDTO> karte) {
		this.karte = karte;
	}


	
}
