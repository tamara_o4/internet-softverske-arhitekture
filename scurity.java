model Korisnik

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private Set<UserPermission> userPermissions = new HashSet<UserPermission>();
	
	public Set<UserPermission> getUserPermissions() {
		return userPermissions;
	}
	public void setUserPermissions(Set<UserPermission> userPermissions) {
		this.userPermissions = userPermissions;
	}
	
	public Korisnik(Long id, String korisnickoIme, String lozinka) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
	}
	
	
korisnik dto
	public KorisnikDTO(Long id, String korisnickoIme, String lozinka) {
		this(id, korisnickoIme, lozinka, new ArrayList<KupovinaDTO>());
	}
	
controller

	@Logged
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured("ROLE_ADMIN")
	
pom
			<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>0.9.0</version>
		</dependency>