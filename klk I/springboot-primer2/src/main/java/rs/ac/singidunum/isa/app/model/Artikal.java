package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;


@Entity
public class Artikal {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;					// Long zbo identity, objekat je zato sto moze biti null
	
	// Lob -large obj - sam odredjuje koji je tip potreban
	@Lob
	private String naziv;
	// not null zato sto je primitivan tip
	private double cena;
	@Lob
	//@Column(columnDefinition = "TINYTEX", insertable = true)
	private String opis;
	
	// moramo dodati zvog veze sa Kupovinom
	@OneToMany(mappedBy = "artikal")
	private Set<Kupovina> kupovine = new HashSet<Kupovina>();

	public Artikal() {	
	}
	
	public Artikal(Long id, String naziv, double cena, String opis) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.cena = cena;
		this.opis = opis;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Set<Kupovina> getKupovine() {
		return kupovine;
	}

	public void setKupovine(Set<Kupovina> kupovine) {
		this.kupovine = kupovine;
	}
	
	
}
