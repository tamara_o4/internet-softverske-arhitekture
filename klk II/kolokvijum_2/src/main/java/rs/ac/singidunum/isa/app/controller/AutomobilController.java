package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.aspect.Logged;
import rs.ac.singidunum.isa.app.dto.AutobmobilDTO;
import rs.ac.singidunum.isa.app.model.Automobil;
import rs.ac.singidunum.isa.app.service.AutomobilService;

@Controller
@RequestMapping(path = "/api/automobili")
public class AutomobilController {
	@Autowired
	AutomobilService automobilService;
	
	@Logged
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<Iterable<AutobmobilDTO>> getAllKorisnici() {
		ArrayList<AutobmobilDTO> registracije = new ArrayList<AutobmobilDTO>();
		for (Automobil automobil : automobilService.findAll()) {
			registracije.add(
					new AutobmobilDTO(automobil.getId(), automobil.getNaziv(), automobil.getCena(), automobil.getKolicina(), null));
		}

		return new ResponseEntity<Iterable<AutobmobilDTO>>(registracije, HttpStatus.OK);
	}

	@Logged
	@RequestMapping(path = "/{automobilId}", method = RequestMethod.GET)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<AutobmobilDTO> getAutomobil(@PathVariable("automobilId") Long automobilId) {
		Optional<Automobil> automobil = automobilService.findOne(automobilId);

		AutobmobilDTO automobilDTO;

		if (automobil.isPresent()) {
			automobilDTO = new AutobmobilDTO(automobil.get().getId(), automobil.get().getNaziv(),
					automobil.get().getCena(), automobil.get().getKolicina(), null);

			return new ResponseEntity<AutobmobilDTO>(automobilDTO, HttpStatus.OK);
		}
		return new ResponseEntity<AutobmobilDTO>(HttpStatus.NOT_FOUND);
	}

	@Logged
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Automobil> createAutomobil(@RequestBody Automobil automobil) {
		try {
			automobilService.save(automobil);
			return new ResponseEntity<Automobil>(automobil, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Automobil>(HttpStatus.BAD_REQUEST);
	}

	@Logged
	@RequestMapping(path = "/{automobilId}", method = RequestMethod.PUT)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<AutobmobilDTO> updateAutomobil(@PathVariable("automobilId") Long automobilId,
			@RequestBody Automobil izmenjeni) {
		Automobil automobil = automobilService.findOne(automobilId).orElse(null);
		if (automobil != null) {
			izmenjeni.setId(automobilId);
			izmenjeni = automobilService.save(izmenjeni);

			AutobmobilDTO izmenjenDto = new AutobmobilDTO(automobil.getId(), automobil.getNaziv(), automobil.getCena(), automobil.getKolicina());
			return new ResponseEntity<AutobmobilDTO>(izmenjenDto, HttpStatus.OK);
		}
		return new ResponseEntity<AutobmobilDTO>(HttpStatus.NOT_FOUND);
	}

	@Logged
	@RequestMapping(path = "/{automobilId}", method = RequestMethod.DELETE)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<Automobil> deleteAutomobil(@PathVariable("automobilId") Long automobilId) {
		if (automobilService.findOne(automobilId).isPresent()) {
			automobilService.delete(automobilId);
			return new ResponseEntity<Automobil>(HttpStatus.OK);
		}
		return new ResponseEntity<Automobil>(HttpStatus.NOT_FOUND);
	}
	


}
