package rs.ac.singidunum.isa.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Kupovina {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	
	@ManyToOne(optional = false)
	private Artikal artikal;
	
	@ManyToOne(optional = false)
	private Korisnik korisnik;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datumKupovine;

	public Kupovina() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Kupovina(Long id, Artikal artikal, Korisnik korisnik, Date datumKupovine) {
		super();
		this.id = id;
		this.artikal = artikal;
		this.korisnik = korisnik;
		this.datumKupovine = datumKupovine;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Artikal getArtikal() {
		return artikal;
	}

	public void setArtikal(Artikal artikal) {
		this.artikal = artikal;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Date getDatumKupovine() {
		return datumKupovine;
	}

	public void setDatumKupovine(Date datumKupovine) {
		this.datumKupovine = datumKupovine;
	}
}
