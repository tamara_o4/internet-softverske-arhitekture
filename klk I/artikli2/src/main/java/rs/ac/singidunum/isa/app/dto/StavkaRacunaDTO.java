package rs.ac.singidunum.isa.app.dto;


public class StavkaRacunaDTO {
	private Long id;	
	
	private double kolicina;
	private double cena;
	
	private RacunDTO racun;
	private ArtikalUMaloprodajiDTO artikalUMaloprodaji;
	public StavkaRacunaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public StavkaRacunaDTO(Long id, double kolicina, double cena, RacunDTO racun,
			ArtikalUMaloprodajiDTO artikalUMaloprodaji) {
		super();
		this.id = id;
		this.kolicina = kolicina;
		this.cena = cena;
		this.racun = racun;
		this.artikalUMaloprodaji = artikalUMaloprodaji;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getKolicina() {
		return kolicina;
	}
	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public RacunDTO getRacun() {
		return racun;
	}
	public void setRacun(RacunDTO racun) {
		this.racun = racun;
	}
	public ArtikalUMaloprodajiDTO getArtikalUMaloprodaji() {
		return artikalUMaloprodaji;
	}
	public void setArtikalUMaloprodaji(ArtikalUMaloprodajiDTO artikalUMaloprodaji) {
		this.artikalUMaloprodaji = artikalUMaloprodaji;
	}
	
	
	
}
