package rs.ac.singidunum.isa.app.dto;

public class StudijskiProgramDTO {
	private Long id;
	private String naziv;
	
	private NastavnikDTO nastavnik;
	private FakultetDTO fakultet;
	
	public StudijskiProgramDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudijskiProgramDTO(String naziv, NastavnikDTO nastavnik, FakultetDTO fakultet) {
		super();
		this.naziv = naziv;
		this.nastavnik = nastavnik;
		this.fakultet = fakultet;
	}
	
	public StudijskiProgramDTO(String naziv) {
		this(naziv, null, null);
	}

	public StudijskiProgramDTO(Long id, String naziv) {
		// dodato zbog kontrolera
		super();
		this.id = id;
		this.naziv = naziv;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public NastavnikDTO getNastavnikDTO() {
		return nastavnik;
	}

	public void setNastavnikDTO(NastavnikDTO nastavnik) {
		this.nastavnik = nastavnik;
	}

	public FakultetDTO getFakultetDTO() {
		return fakultet;
	}

	public void setFakultetDTO(FakultetDTO fakultet) {
		this.fakultet = fakultet;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public FakultetDTO getFakultet() {
		return fakultet;
	}

	public void setFakultet(FakultetDTO fakultet) {
		this.fakultet = fakultet;
	}
	
	
}
