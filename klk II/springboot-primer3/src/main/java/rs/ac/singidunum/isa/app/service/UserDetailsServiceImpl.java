package rs.ac.singidunum.isa.app.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rs.ac.singidunum.isa.app.model.Korisnik;
import rs.ac.singidunum.isa.app.model.UserPermission;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	KorisnikService korisnikService;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// Dobavljanje korisnika po korisnickom imenu.
		Optional<Korisnik> korisnik = korisnikService.findByKorisnickoIme(username);
		
		if(korisnik.isPresent()) {
			// Formiranje liste dodeljenih prava pristupa.
			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			for(UserPermission userPermission : korisnik.get().getUserPermission()) {
				grantedAuthorities.add(new SimpleGrantedAuthority(userPermission.getPermission().getTitle()));
			}
			
			// Kreiranje korisnika na osnovu korisnickog imena, lozinke i dodeljenih prava pristupa.
			return new org.springframework.security.core.userdetails.User(korisnik.get().getKorisnickoIme(), korisnik.get().getLozinka(), grantedAuthorities);
		}
		return null;
	}

}
