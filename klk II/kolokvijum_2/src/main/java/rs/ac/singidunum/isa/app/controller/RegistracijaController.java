package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.aspect.Logged;
import rs.ac.singidunum.isa.app.dto.RegistracijaDTO;
import rs.ac.singidunum.isa.app.model.Registracija;
import rs.ac.singidunum.isa.app.service.RegistracijaSerivice;

@Controller
@RequestMapping(path = "/api/registracije")
public class RegistracijaController {
	@Autowired
	RegistracijaSerivice registracijaService;
	
	@Logged
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured("ROLE_VLASNIK")
	public ResponseEntity<Iterable<RegistracijaDTO>> getAllKorisnici() {
		ArrayList<RegistracijaDTO> registracije = new ArrayList<RegistracijaDTO>();
		for (Registracija registracija : registracijaService.findAll()) {
			registracije.add(
					new RegistracijaDTO(registracija.getId(), registracija.getRegistracioniBroj(), registracija.getDatumRegistracija(), registracija.getStatus(), null, null));
		}

		return new ResponseEntity<Iterable<RegistracijaDTO>>(registracije, HttpStatus.OK);
	}

	@RequestMapping(path = "/{registracijaId}", method = RequestMethod.GET)
	public ResponseEntity<RegistracijaDTO> getRegistracija(@PathVariable("registracijaId") Long registracijaId) {
		Optional<Registracija> registracija = registracijaService.findOne(registracijaId);

		RegistracijaDTO registracijaDTO;

		if (registracija.isPresent()) {
			registracijaDTO = new RegistracijaDTO(registracija.get().getId(), registracija.get().getRegistracioniBroj(),
					registracija.get().getDatumRegistracija(), registracija.get().getStatus(), null, null);

			return new ResponseEntity<RegistracijaDTO>(registracijaDTO, HttpStatus.OK);
		}
		return new ResponseEntity<RegistracijaDTO>(HttpStatus.NOT_FOUND);
	}

	@Logged
	@RequestMapping(path = "", method = RequestMethod.POST)
	@Secured("ROLE_VLASNIK")
	public ResponseEntity<Registracija> createRegistracija(@RequestBody Registracija registracija) {
		try {
			registracijaService.save(registracija);
			return new ResponseEntity<Registracija>(registracija, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Registracija>(HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(path = "/{registracijaId}", method = RequestMethod.PUT)
	public ResponseEntity<RegistracijaDTO> updateRegistracija(@PathVariable("registracijaId") Long registracijaId,
			@RequestBody Registracija izmenjeni) {
		Registracija registracija = registracijaService.findOne(registracijaId).orElse(null);
		if (registracija != null) {
			izmenjeni.setId(registracijaId);
			izmenjeni = registracijaService.save(izmenjeni);

			RegistracijaDTO izmenjenDto = new RegistracijaDTO(registracija.getId(), registracija.getRegistracioniBroj(), registracija.getDatumRegistracija(), registracija.getStatus(), null, null);
			return new ResponseEntity<RegistracijaDTO>(izmenjenDto, HttpStatus.OK);
		}
		return new ResponseEntity<RegistracijaDTO>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(path = "/{registracijaId}", method = RequestMethod.DELETE)
	public ResponseEntity<Registracija> deleteRegistracija(@PathVariable("registracijaId") Long registracijaId) {
		if (registracijaService.findOne(registracijaId).isPresent()) {
			registracijaService.delete(registracijaId);
			return new ResponseEntity<Registracija>(HttpStatus.OK);
		}
		return new ResponseEntity<Registracija>(HttpStatus.NOT_FOUND);
	}
	

}
