package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.StavkaRacuna;
import rs.ac.singidunum.isa.app.repository.StavkaRacunaRepository;

@Service
public class StavkaRacunaService {
	@Autowired
	private StavkaRacunaRepository stavkaRacunaRepository;

	public StavkaRacunaService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StavkaRacunaService(StavkaRacunaRepository stavkaRacunaRepository) {
		super();
		this.stavkaRacunaRepository = stavkaRacunaRepository;
	}

	public StavkaRacunaRepository getStavkaRacunaRepository() {
		return stavkaRacunaRepository;
	}

	public void setStavkaRacunaRepository(StavkaRacunaRepository stavkaRacunaRepository) {
		this.stavkaRacunaRepository = stavkaRacunaRepository;
	}
	public Iterable<StavkaRacuna> findAll(){
		return stavkaRacunaRepository.findAll();
	}
	
	public Optional<StavkaRacuna> findOne(Long id) {
		return stavkaRacunaRepository.findById(id);
	}
	
	public StavkaRacuna save(StavkaRacuna artikal){
		return stavkaRacunaRepository.save(artikal);
	}
		
	public void delete(Long id) {
		stavkaRacunaRepository.deleteById(id);
	}
	
	public void delete(StavkaRacuna artikal) {
		stavkaRacunaRepository.delete(artikal);
	}
	
	
}
