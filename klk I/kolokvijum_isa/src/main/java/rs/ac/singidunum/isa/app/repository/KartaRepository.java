package rs.ac.singidunum.isa.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Karta;

@Repository
public interface KartaRepository extends CrudRepository<Karta, Long> {

	//@Query("SELECT a FROM Karta a WHERE a.putnik = :id")
	List<Karta> findByPutnik(Long id);
	
}
