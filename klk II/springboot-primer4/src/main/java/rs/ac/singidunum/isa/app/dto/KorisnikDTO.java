package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class KorisnikDTO {
	private Long id;
	private String korisnickoIme;
	private String lozinka;
	private ArrayList<KupovinaDTO> kupovine = new ArrayList<KupovinaDTO>();

	public KorisnikDTO() {
		super();
	}

	public KorisnikDTO(Long id, String korisnickoIme, String lozinka, ArrayList<KupovinaDTO> kupovine) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.kupovine = kupovine;
	}
	
	public KorisnikDTO(Long id, String korisnickoIme, String lozinka) {
		this(id, korisnickoIme, lozinka, new ArrayList<KupovinaDTO>());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public ArrayList<KupovinaDTO> getKupovine() {
		return kupovine;
	}

	public void setKupovine(ArrayList<KupovinaDTO> kupovine) {
		this.kupovine = kupovine;
	}

}
