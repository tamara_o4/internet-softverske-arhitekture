package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import rs.ac.singidunum.isa.app.dto.NastavniklDTO;
import rs.ac.singidunum.isa.app.dto.ZvanjeDTO;

import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.Zvanje;
import rs.ac.singidunum.isa.app.service.NastavnikService;

@Controller
@RequestMapping(path = "/api/nastavnici")
public class NastavnikController {
	@Autowired
	private NastavnikService nastavnikService; 	
	
	// PRIKAZ SVIH ARTIKALA
		@RequestMapping(path = "", method = RequestMethod.GET)
		public ResponseEntity<Iterable<NastavniklDTO>> getAllArtikli() {
			ArrayList<NastavniklDTO> nastavnici = new ArrayList<NastavniklDTO>();
			for(Nastavnik nastavnik : nastavnikService.findAll()) {
				ArrayList<ZvanjeDTO> zvanja = new ArrayList<ZvanjeDTO>();
				for(Zvanje zvanje : nastavnik.getZvanja()) {
					zvanja.add(new ZvanjeDTO(zvanje.getId(), zvanje.getDatumIzbora(), zvanje.getDatumPrestanka(), null, zvanje.getNaucneOblasti(),null));
				}
				nastavnici.add(new NastavniklDTO(nastavnik.getId(), nastavnik.getIme(), nastavnik.getBiografija(), nastavnik.getJmbg(), zvanja));
			}
			
			return new ResponseEntity<Iterable<NastavniklDTO>>(nastavnici, HttpStatus.OK);
//			return new ResponseEntity<List<Artikal>>(artikalService.findByPriceBetween(min, max), HttpStatus.OK);	// mora da se uveze da ne bi bio null pointer u Artikli service
		}
		
}
