package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Popust;
import rs.ac.singidunum.isa.app.repository.PopustRepository;

@Service
public class PopustService {
	@Autowired
	private PopustRepository popustRepository;

	public PopustService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PopustService(PopustRepository popustRepository) {
		super();
		this.popustRepository = popustRepository;
	}

	public PopustRepository getPopustRepository() {
		return popustRepository;
	}

	public void setPopustRepository(PopustRepository popustRepository) {
		this.popustRepository = popustRepository;
	}
	
	public Iterable<Popust> findAll(){
		return popustRepository.findAll();
	}
	
	public Optional<Popust> findOne(Long id) {
		return popustRepository.findById(id);
	}
	
	public Popust save(Popust artikal){
		return popustRepository.save(artikal);
	}
		
	public void delete(Long id) {
		popustRepository.deleteById(id);
	}
	
	public void delete(Popust artikal) {
		popustRepository.delete(artikal);
	}
}
