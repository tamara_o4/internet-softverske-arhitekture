package rs.ac.singidunum.isa.app.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Zvanje {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datumIzbora;
	private Date datumPrestanka;
	
	@ManyToOne(optional = false)
	private Nastavnik nastavnik;
	
	@OneToMany(mappedBy = "zvanje")
	private Set<NaucnaOblast> naucneOblasti = new HashSet<NaucnaOblast>();	
	
	@OneToMany(mappedBy = "zvanje")
	private Set<TipZvanja> zvanja = new HashSet<TipZvanja>();

	public Zvanje() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Zvanje(Long id, Date datumIzbora, Date datumPrestanka, Nastavnik nastavnik, Set<NaucnaOblast> naucneOblasti,
			Set<TipZvanja> zvanja) {
		super();
		this.id = id;
		this.datumIzbora = datumIzbora;
		this.datumPrestanka = datumPrestanka;
		this.nastavnik = nastavnik;
		this.naucneOblasti = naucneOblasti;
		this.zvanja = zvanja;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatumIzbora() {
		return datumIzbora;
	}

	public void setDatumIzbora(Date datumIzbora) {
		this.datumIzbora = datumIzbora;
	}

	public Date getDatumPrestanka() {
		return datumPrestanka;
	}

	public void setDatumPrestanka(Date datumPrestanka) {
		this.datumPrestanka = datumPrestanka;
	}

	public Nastavnik getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}

	public Set<NaucnaOblast> getNaucneOblasti() {
		return naucneOblasti;
	}

	public void setNaucneOblasti(Set<NaucnaOblast> naucneOblasti) {
		this.naucneOblasti = naucneOblasti;
	}

	public Set<TipZvanja> getZvanja() {
		return zvanja;
	}

	public void setZvanja(Set<TipZvanja> zvanja) {
		this.zvanja = zvanja;
	}
	


//	
//	@ManyToOne(optional = false)
//	private Korisnik korisnik;
//	
//	@Temporal(TemporalType.TIMESTAMP)
//	private Date datumKupovine;
	
	
	
}
