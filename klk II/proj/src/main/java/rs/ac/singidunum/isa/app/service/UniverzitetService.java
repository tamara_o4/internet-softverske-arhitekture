package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Univerzitet;
import rs.ac.singidunum.isa.app.repository.UniverzitetRepository;

@Service
public class UniverzitetService {
	@Autowired
	private UniverzitetRepository kartaRepository;

	public UniverzitetService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UniverzitetService(UniverzitetRepository kartaRepository) {
		super();
		this.kartaRepository = kartaRepository;
	}

	public UniverzitetRepository getUniverzitetRepository() {
		return kartaRepository;
	}

	public void setUniverzitetRepository(UniverzitetRepository kartaRepository) {
		this.kartaRepository = kartaRepository;
	}
	
	public Iterable<Univerzitet> findAll(){
		return kartaRepository.findAll();
	}
	
	public Optional<Univerzitet> findOne(Long id) {
		return kartaRepository.findById(id);
	}
	
	public Univerzitet save(Univerzitet korisnik){
		return kartaRepository.save(korisnik);
	}
		
	public void delete(Long id) {
		 kartaRepository.deleteById(id);
	}
	
	public void delete(Univerzitet korisnik) {
		 kartaRepository.delete(korisnik);
	}
	
	
}
