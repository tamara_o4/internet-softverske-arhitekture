package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Transakcija;
import rs.ac.singidunum.isa.app.repository.TransakcijaRepository;

@Service
public class TransakcijaService {
	@Autowired
	private TransakcijaRepository transakcijaRepository;
	
	public Iterable<Transakcija> findAll(){
		return transakcijaRepository.findAll();
	}
	
	public Optional<Transakcija> findOne(Long id) {
		return transakcijaRepository.findById(id);
	}
	
	public Transakcija save(Transakcija korisnik){
		return transakcijaRepository.save(korisnik);
	}
		
	public void delete(Long id) {
		 transakcijaRepository.deleteById(id);
	}
	
	public void delete(Transakcija korisnik) {
		 transakcijaRepository.delete(korisnik);
	}
}
