package rs.ac.singidunum.isa.app.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;



import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenUtils {
	private String secret = "123456";
	
	// provera da li je to taj token
	
	private Claims getClaims(String token) {

		Claims claims = null;
		try {
			claims = Jwts.parser().setSigningKey("123456").parseClaimsJws(token).getBody();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return claims;
	}

	// provera da li je token istekao
	private boolean isExired(String token) {
		try {
			return getClaims(token).getExpiration().before(new Date(System.currentTimeMillis()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		return true;
	}
	
	// dobavljanje korisnickog imena
	public String getUsername(String token) {
		String username = null;
		try {
			getClaims(token).getSubject();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return username;
	}
	
	// metoda za validaciju tokena, da li je taj token taj i da li je vreme to
	public boolean validateToken(String token, UserDetails userDetails) {
		String username = getUsername(token);
		return username.equals(userDetails.getUsername()) && isExired(token);
	}
	
	//
	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<String, Object>();
		claims.put("sub", userDetails.getUsername());
		claims.put("created", new Date(System.currentTimeMillis()));
		
		return Jwts.builder().setClaims(claims).setExpiration(new Date(System.currentTimeMillis() + 7200 * 1000)).signWith(SignatureAlgorithm.HS512, secret).compact();
	}
	
}
