package rs.ac.singidunum.isa.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Adresa {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String ulica;
	@Lob
	private String broj;
	
	@ManyToOne(optional = false)
	private Mesto mesto;
	
	@OneToOne(mappedBy = "adresa")
	private Univerzitet univerzitet;
	
	@OneToOne(mappedBy = "adresa")
	private Fakultet fakultet;
	
	@OneToMany(mappedBy = "adresa")
	private Nastavnik nastavnik;
	
	@OneToMany(mappedBy = "adresa")
	private Student student;
	
	public Adresa() {
		super();
	}

	public Adresa(Long id, String ulica, String broj, Mesto mesto, Univerzitet univerzitet, Fakultet fakultet,
			Nastavnik nastavnik, Student student) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
		this.mesto = mesto;
		this.univerzitet = univerzitet;
		this.fakultet = fakultet;
		this.nastavnik = nastavnik;
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public Mesto getMesto() {
		return mesto;
	}

	public void setMesto(Mesto mesto) {
		this.mesto = mesto;
	}

	public Univerzitet getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(Univerzitet univerzitet) {
		this.univerzitet = univerzitet;
	}

	public Fakultet getFakultet() {
		return fakultet;
	}

	public void setFakultet(Fakultet fakultet) {
		this.fakultet = fakultet;
	}

	public Nastavnik getNastavnik() {
		return nastavnik;
	}

	public void setNastavnik(Nastavnik nastavnik) {
		this.nastavnik = nastavnik;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

}
