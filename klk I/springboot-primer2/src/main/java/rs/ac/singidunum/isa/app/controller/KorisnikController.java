package rs.ac.singidunum.isa.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.model.Korisnik;
import rs.ac.singidunum.isa.app.service.KorisnikSerivice;

@Controller
@RequestMapping(path = "/api/korisnici")
public class KorisnikController {
	@Autowired
	private KorisnikSerivice korisniklService; 	
	
	// PRIKAZ SVIH
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Korisnik>> getallKOrisnici() {
		return new ResponseEntity<Iterable<Korisnik>>(korisniklService.findAll(), HttpStatus.OK);
	}
		
}
