package rs.ac.singidunum.isa.app.dto;

import java.util.Date;

import rs.ac.singidunum.isa.app.model.FinansijskaKartica;


public class TransakcijaDTO {
	private Long id;	
	private Double iznos;
	private Date datumValute;
	private FinansijskaKartica finansijskaKartica;
	public TransakcijaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TransakcijaDTO(Long id, Double iznos, Date datumValute, FinansijskaKartica finansijskaKartica) {
		super();
		this.id = id;
		this.iznos = iznos;
		this.datumValute = datumValute;
		this.finansijskaKartica = finansijskaKartica;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getIznos() {
		return iznos;
	}
	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}
	public Date getDatumValute() {
		return datumValute;
	}
	public void setDatumValute(Date datumValute) {
		this.datumValute = datumValute;
	}
	public FinansijskaKartica getFinansijskaKartica() {
		return finansijskaKartica;
	}
	public void setFinansijskaKartica(FinansijskaKartica finansijskaKartica) {
		this.finansijskaKartica = finansijskaKartica;
	}
	
	
}
