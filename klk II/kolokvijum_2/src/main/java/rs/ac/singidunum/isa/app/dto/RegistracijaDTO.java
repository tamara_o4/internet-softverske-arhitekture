package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import rs.ac.singidunum.isa.app.model.Automobil;
import rs.ac.singidunum.isa.app.model.Korisnik;

public class RegistracijaDTO {
	private Long id;
	
	private String registracioniBroj;
	
	private Date datumRegistracija;
	
	private String status;
	
	private Korisnik korisnik;
	private Automobil automobil;
	public RegistracijaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RegistracijaDTO(Long id, String registracioniBroj, Date datumRegistracija, String status, Korisnik korisnik,
			Automobil automobil) {
		super();
		this.id = id;
		this.registracioniBroj = registracioniBroj;
		this.datumRegistracija = datumRegistracija;
		this.status = status;
		this.korisnik = korisnik;
		this.automobil = automobil;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRegistracioniBroj() {
		return registracioniBroj;
	}
	public void setRegistracioniBroj(String registracioniBroj) {
		this.registracioniBroj = registracioniBroj;
	}
	public Date getDatumRegistracija() {
		return datumRegistracija;
	}
	public void setDatumRegistracija(Date datumRegistracija) {
		this.datumRegistracija = datumRegistracija;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Korisnik getKorisnik() {
		return korisnik;
	}
	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}
	public Automobil getAutomobil() {
		return automobil;
	}
	public void setAutomobil(Automobil automobil) {
		this.automobil = automobil;
	}

	

}
