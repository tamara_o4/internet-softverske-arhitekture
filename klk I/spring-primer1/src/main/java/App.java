import repository.ArtikalRepository;
import service.ArtikalService;
import view.ArtikalView;

public class App {

	public static void main(String[] args) {
		ArtikalRepository ar = new ArtikalRepository();
		ArtikalService as = new ArtikalService(ar);
		ArtikalView av = new ArtikalView(as);
		av.show();
	}

}
