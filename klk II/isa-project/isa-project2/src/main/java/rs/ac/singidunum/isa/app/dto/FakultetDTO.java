package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class FakultetDTO {
	private Long id;
	private String naziv;
	private UniverzitetDTO univerzitet;
	private AdresaDTO adresal;
	private NastavnikDTO dekan;
	private ArrayList<StudijskiProgramDTO> studijskiProgrami = new ArrayList<StudijskiProgramDTO>();
	
	public FakultetDTO() {
		super();
	}

	public FakultetDTO(Long id, String naziv, UniverzitetDTO univerzitet) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.univerzitet = univerzitet;
	}
	
	
	public FakultetDTO(Long id, String naziv, UniverzitetDTO univerzitet, AdresaDTO adresal) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.univerzitet = univerzitet;
		this.adresal = adresal;
	}
	
	public FakultetDTO(Long id, String naziv, UniverzitetDTO univerzitet, AdresaDTO adresal, NastavnikDTO dekan,
			ArrayList<StudijskiProgramDTO> studijskiProgrami) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.univerzitet = univerzitet;
		this.adresal = adresal;
		this.dekan = dekan;
		this.studijskiProgrami = studijskiProgrami;
	}
	
	public FakultetDTO(Long id, String naziv, UniverzitetDTO univerzitet, AdresaDTO adresal, NastavnikDTO dekan) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.univerzitet = univerzitet;
		this.adresal = adresal;
		this.dekan = dekan;
		new ArrayList<StudijskiProgramDTO>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public UniverzitetDTO getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(UniverzitetDTO univerzitet) {
		this.univerzitet = univerzitet;
	}

	public AdresaDTO getAdresal() {
		return adresal;
	}

	public void setAdresal(AdresaDTO adresal) {
		this.adresal = adresal;
	}

	public NastavnikDTO getDekan() {
		return dekan;
	}

	public void setDekan(NastavnikDTO dekan) {
		this.dekan = dekan;
	}

	public ArrayList<StudijskiProgramDTO> getStudijskiProgrami() {
		return studijskiProgrami;
	}

	public void setStudijskiProgrami(ArrayList<StudijskiProgramDTO> studijskiProgrami) {
		this.studijskiProgrami = studijskiProgrami;
	}
	
}
