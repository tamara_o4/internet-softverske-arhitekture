package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Korisnik;
import rs.ac.singidunum.isa.app.repository.KorisnikRepository;

@Service
public class KorisnikSerivice {
	@Autowired
	private KorisnikRepository korisnikRepository;

	public KorisnikSerivice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KorisnikSerivice(KorisnikRepository korisnikRepository) {
		super();
		this.korisnikRepository = korisnikRepository;
	}

	public KorisnikRepository getKorisnikRepository() {
		return korisnikRepository;
	}

	public void setKorisnikRepository(KorisnikRepository korisnikRepository) {
		this.korisnikRepository = korisnikRepository;
	}
	
	public Iterable<Korisnik> findAll(){
		return korisnikRepository.findAll();
	}
	
	public Optional<Korisnik> findOne(Long id) {
		return korisnikRepository.findById(id);
	}
	
	//
//	public List<Artikal> findByPriceBetween(double min, double max){
//		return artikalRepository.findByPriceBetween(min, max);
//	}
	
	public Korisnik save(Korisnik korisnik){
		return korisnikRepository.save(korisnik);
	}
		
	public void delete(Long id) {
		 korisnikRepository.deleteById(id);
	}
	
	public void delete(Korisnik korisnik) {
		 korisnikRepository.delete(korisnik);
	}
	
	
}
