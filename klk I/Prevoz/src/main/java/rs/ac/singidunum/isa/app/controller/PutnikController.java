package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.dto.KartaDTO;
import rs.ac.singidunum.isa.app.dto.PutnikDTO;
import rs.ac.singidunum.isa.app.dto.StanicaDTO;
import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.model.Putnik;
import rs.ac.singidunum.isa.app.service.PutnikSerivice;


@Controller
@RequestMapping(path = "/api/putnici")
public class PutnikController {
	@Autowired
	private PutnikSerivice putnikService;
	
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<PutnikDTO>> getAllPutnik(@RequestParam(name = "id", required = false) Long id){
		ArrayList<PutnikDTO> putnici = new ArrayList<PutnikDTO>();
		if (id == null) {
			for(Putnik putnik : putnikService.findAll()) {
				ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
				for (Karta karta : putnik.getKarte()) {
					StanicaDTO stanica = new StanicaDTO(karta.getStanica().getId(), karta.getStanica().getNaziv(), karta.getStanica().getAdresa());
					karte.add(new KartaDTO(karta.getId(), karta.getDatumVremePolaska(), karta.isPrtljag(), karta.getCena(), null, stanica));
				}
				putnici.add(new PutnikDTO(putnik.getId(), putnik.getIme(), putnik.getPrezime(), karte));
			}
		}
		else {
			ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
			for(Karta karta : putnikService.findByStanicaId(id)) {
				StanicaDTO stanica = new StanicaDTO(karta.getStanica().getId(), karta.getStanica().getNaziv(), karta.getStanica().getAdresa());
				karte.add(new KartaDTO(karta.getId(), karta.getDatumVremePolaska(), karta.isPrtljag(), karta.getCena(), null, stanica));
				PutnikDTO putnik = new PutnikDTO(karta.getPutnik().getId(), karta.getPutnik().getIme(), karta.getPutnik().getPrezime());
				putnici.add(new PutnikDTO(putnik.getId(), putnik.getIme(), putnik.getPrezime(), karte));
			}
		}
		return new ResponseEntity<Iterable<PutnikDTO>>(putnici, HttpStatus.OK);
	}
	
	
//	// brisanje
//	@RequestMapping(path = "/{stanicaId}", method = RequestMethod.DELETE)
//	public ResponseEntity<Stanica> deleteKarta(@PathVariable("stanicaId") Long stanicaId) {
//		if (stanicaService.findOne(stanicaId).isPresent()) {
//			stanicaService.delete(stanicaId);
//			return new ResponseEntity<Stanica>(HttpStatus.OK);
//		}
//		return new ResponseEntity<Stanica>(HttpStatus.NOT_FOUND);
//	}
	
}
