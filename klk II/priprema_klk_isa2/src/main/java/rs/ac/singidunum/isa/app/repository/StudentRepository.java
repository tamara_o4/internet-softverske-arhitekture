package rs.ac.singidunum.isa.app.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Student;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long>{
	Optional<Student> findByIndeks(String indeks);
}
