package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class FinansijskaKartica {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Lob
	private String pozivNaBroj;
	private Double stanje;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "student_id")
    private Student student;
	
	@OneToMany(mappedBy = "finansijskaKartica")
	private Set<Transakcija> karte = new HashSet<Transakcija>();

	
	
	
	public FinansijskaKartica() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FinansijskaKartica(Long id, String pozivNaBroj, Double stanje, Student student, Set<Transakcija> karte) {
		super();
		this.id = id;
		this.pozivNaBroj = pozivNaBroj;
		this.stanje = stanje;
		this.student = student;
		this.karte = karte;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPozivNaBroj() {
		return pozivNaBroj;
	}

	public void setPozivNaBroj(String pozivNaBroj) {
		this.pozivNaBroj = pozivNaBroj;
	}

	public Double getStanje() {
		return stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Set<Transakcija> getKarte() {
		return karte;
	}

	public void setKarte(Set<Transakcija> karte) {
		this.karte = karte;
	}

	
	
	
	
	
	
}