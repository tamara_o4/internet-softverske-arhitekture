package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Fakultet {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String naziv;
	
	@ManyToOne(optional = false)
	private Univerzitet univerzitet;
	
	@OneToOne
	private Adresa adresa;
	
	@OneToOne
	private Nastavnik dekan;
	
	@OneToMany(mappedBy = "fakultet")
	private Set<StudijskiProgram> studijskiProgrami = new HashSet<StudijskiProgram>();

	public Fakultet() {
		super();
	}

	public Fakultet(Long id, String naziv, Univerzitet univerzitet, Adresa adresa, Nastavnik dekan) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.univerzitet = univerzitet;
		this.adresa = adresa;
		this.dekan = dekan;
	}

	public Fakultet(Long id, String naziv, Univerzitet univerzitet, Adresa adresa, Nastavnik dekan,
			Set<StudijskiProgram> studijskiProgrami) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.univerzitet = univerzitet;
		this.adresa = adresa;
		this.dekan = dekan;
		this.studijskiProgrami = studijskiProgrami;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Univerzitet getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(Univerzitet univerzitet) {
		this.univerzitet = univerzitet;
	}

	public Adresa getAdresa() {
		return adresa;
	}

	public void setAdresa(Adresa adresa) {
		this.adresa = adresa;
	}

	public Nastavnik getDekan() {
		return dekan;
	}

	public void setDekan(Nastavnik dekan) {
		this.dekan = dekan;
	}

	public Set<StudijskiProgram> getStudijskiProgrami() {
		return studijskiProgrami;
	}

	public void setStudijskiProgrami(Set<StudijskiProgram> studijskiProgrami) {
		this.studijskiProgrami = studijskiProgrami;
	}
	
}
