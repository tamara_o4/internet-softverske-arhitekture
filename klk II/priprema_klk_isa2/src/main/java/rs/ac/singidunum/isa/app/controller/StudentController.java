package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.StudentDTO;
import rs.ac.singidunum.isa.app.model.Student;
import rs.ac.singidunum.isa.app.service.StudentService;

@Controller
@RequestMapping(path = "/api/studenti")
public class StudentController {
	@Autowired
	private StudentService studentService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<StudentDTO>> getAllStudenti(){
		ArrayList<StudentDTO> studenti = new ArrayList<StudentDTO>();
		for(Student s : studentService.findAll()) {
//			PutnikDTO putnik = new PutnikDTO(karta.getPutnik().getId(), karta.getPutnik().getIme(), karta.getPutnik().getPrezime());
//			StanicaDTO stanica = new StanicaDTO(karta.getStanica().getId(), karta.getStanica().getNaziv(), karta.getStanica().getAdresa());
			studenti.add(new StudentDTO(s.getId(), s.getIndeks(), s.getIme(), s.getPrezime(), s.getEmail(), s.getLozinka(), null));
		}
		
		return new ResponseEntity<Iterable<StudentDTO>>(studenti, HttpStatus.OK);
	}
	
	
	
}
