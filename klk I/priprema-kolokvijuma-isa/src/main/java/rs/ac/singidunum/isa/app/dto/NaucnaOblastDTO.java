package rs.ac.singidunum.isa.app.dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.OneToMany;

import rs.ac.singidunum.isa.app.model.Zvanje;

public class NaucnaOblastDTO {
	private Long id;
	
	private String naziv;
	
	// vise zvanja

	private Zvanje zvanje;
	
	public NaucnaOblastDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	



	public NaucnaOblastDTO(Long id, String naziv, Zvanje zvanje) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.zvanje = zvanje;
	}





	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}





	public Zvanje getZvanje() {
		return zvanje;
	}





	public void setZvanje(Zvanje zvanje) {
		this.zvanje = zvanje;
	}


	
}
