package rs.ac.singidunum.isa.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Nastavnik;
import rs.ac.singidunum.isa.app.model.TipZvanja;
import rs.ac.singidunum.isa.app.model.Zvanje;
import rs.ac.singidunum.isa.app.repository.ZvanjeRepository;

@Service
public class ZvanjeService {
	@Autowired
	private ZvanjeRepository zvanjeRepository;

	public ZvanjeService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ZvanjeService(ZvanjeRepository zvanjeRepository) {
		super();
		this.zvanjeRepository = zvanjeRepository;
	}

	public ZvanjeRepository getZvanjeRepository() {
		return zvanjeRepository;
	}

	public void setZvanjeRepository(ZvanjeRepository zvanjeRepository) {
		this.zvanjeRepository = zvanjeRepository;
	}
	
	public Iterable<Zvanje> findAll(){
		return zvanjeRepository.findAll();
	}
	
	public Optional<Zvanje> findOne(Long id) {
		return zvanjeRepository.findById(id);
	}
	
	//
	
	public Zvanje save(Zvanje zvanje){
		return zvanjeRepository.save(zvanje);
	}
		
	public void delete(Long id) {
		zvanjeRepository.deleteById(id);
	}
	
	public void delete(Zvanje zvanje) {
		zvanjeRepository.delete(zvanje);
	}

	public List<Nastavnik> pronadjiPoZvanju(ArrayList<TipZvanja> tipZvanja){
		return zvanjeRepository.pronadjiPoZvanju(tipZvanja);
	}
	
}
