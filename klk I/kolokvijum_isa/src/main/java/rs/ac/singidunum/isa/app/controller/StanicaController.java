package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.StanicaDTO;
import rs.ac.singidunum.isa.app.model.Stanica;
import rs.ac.singidunum.isa.app.service.StanicaSerivice;

@Controller
@RequestMapping(path = "/api/stanice")
public class StanicaController {
	@Autowired
	private StanicaSerivice stanicaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<StanicaDTO>> getAllArtikli() {
		ArrayList<StanicaDTO> artikli = new ArrayList<StanicaDTO>();
		for (Stanica stanica : stanicaService.findAll()) {
//			ArrayList<KupovinaDTO> kupovine = new ArrayList<KupovinaDTO>();
//			for (Kupovina kupovina : Karta.getKupovine()) {
//				kupovine.add(new KupovinaDTO(kupovina.getId(), null, new KorisnikDTO(kupovina.getKorisnik().getId(),
//						kupovina.getKorisnik().getKorisnickoIme(), null), kupovina.getDatumKupovine()));
//			}
			artikli.add(new StanicaDTO(stanica.getId(), stanica.getNaziv(), stanica.getAdresa()));
		}

		return new ResponseEntity<Iterable<StanicaDTO>>(artikli, HttpStatus.OK);
	}
	
	// pretraga jednog
	@RequestMapping(path = "/{stanicaId}", method = RequestMethod.GET)
	public ResponseEntity<StanicaDTO> getKarta(@PathVariable("stanicaId") Long stanicaId) {
		Optional<Stanica> artikal = stanicaService.findOne(stanicaId);
		StanicaDTO karta;
		if (artikal.isPresent()) {
			karta = new StanicaDTO(artikal.get().getId(), artikal.get().getNaziv(), artikal.get().getAdresa());
			return new ResponseEntity<StanicaDTO>(karta, HttpStatus.OK);
		}
		return new ResponseEntity<StanicaDTO>(HttpStatus.NOT_FOUND);
	
	}
	
	// dodavanje
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Stanica> createStanica(@RequestBody Stanica stanica) {
		try {
			stanicaService.save(stanica);
			System.out.println("LOG");
			return new ResponseEntity<Stanica>(stanica, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Stanica>(HttpStatus.BAD_REQUEST);
	}

	// izmena
	@RequestMapping(path = "/{stanicaId}", method = RequestMethod.PUT)
	public ResponseEntity<Stanica> updateStanica(@PathVariable("stanicaId") Long stanicaId,
			@RequestBody Stanica izmenjeniStanica) {
		Stanica stanica = stanicaService.findOne(stanicaId).orElse(null);
		if (stanica != null) {
			izmenjeniStanica.setId(stanicaId);
			izmenjeniStanica = stanicaService.save(izmenjeniStanica);
			return new ResponseEntity<Stanica>(izmenjeniStanica, HttpStatus.OK);
		}
		return new ResponseEntity<Stanica>(HttpStatus.NOT_FOUND);
	}

	
	// brisanje
	@RequestMapping(path = "/{stanicaId}", method = RequestMethod.DELETE)
	public ResponseEntity<Stanica> deleteStanica(@PathVariable("stanicaId") Long stanicaId) {
		if (stanicaService.findOne(stanicaId).isPresent()) {
			stanicaService.delete(stanicaId);
			return new ResponseEntity<Stanica>(HttpStatus.OK);
		}
		return new ResponseEntity<Stanica>(HttpStatus.NOT_FOUND);
	}
	

	
}
