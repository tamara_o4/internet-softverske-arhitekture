package rs.ac.singidunum.isa.app.dto;

import rs.ac.singidunum.isa.app.model.Zvanje;

public class TipZvanjaDTO {
	private Long id;

	private String naziv;
	private Zvanje zvanje;
	public TipZvanjaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TipZvanjaDTO(Long id, String naziv, Zvanje zvanje) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.zvanje = zvanje;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public Zvanje getZvanje() {
		return zvanje;
	}
	public void setZvanje(Zvanje zvanje) {
		this.zvanje = zvanje;
	}

	
}
