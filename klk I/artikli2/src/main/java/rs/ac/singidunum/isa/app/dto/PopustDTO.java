package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

public class PopustDTO {
	
	private Long id;					// Long zbo identity, objekat je zato sto moze biti null
	
	private String naziv;
	private Date pocetak;

	private Date kraj;
	
	private double procenat;
	private ArrayList<ArtikalUMaloprodajiDTO> artikliUmaloprodiji = new ArrayList<ArtikalUMaloprodajiDTO>();
	
	public PopustDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public PopustDTO(Long id, String naziv, Date pocetak, Date kraj, double procenat,
			ArrayList<ArtikalUMaloprodajiDTO> artikliUmaloprodiji) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.procenat = procenat;
		this.artikliUmaloprodiji = artikliUmaloprodiji;
	}
	
	public PopustDTO(Long id, String naziv, Date pocetak, Date kraj, double procenat) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.pocetak = pocetak;
		this.kraj = kraj;
		this.procenat = procenat;
		new HashSet<ArtikalUMaloprodajiDTO>();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public Date getPocetak() {
		return pocetak;
	}
	public void setPocetak(Date pocetak) {
		this.pocetak = pocetak;
	}
	public Date getKraj() {
		return kraj;
	}
	public void setKraj(Date kraj) {
		this.kraj = kraj;
	}
	public double getProcenat() {
		return procenat;
	}
	public void setProcenat(double procenat) {
		this.procenat = procenat;
	}
	public ArrayList<ArtikalUMaloprodajiDTO> getArtikliUmaloprodiji() {
		return artikliUmaloprodiji;
	}
	public void setArtikliUmaloprodiji(ArrayList<ArtikalUMaloprodajiDTO> artikliUmaloprodiji) {
		this.artikliUmaloprodiji = artikliUmaloprodiji;
	}

	
}
