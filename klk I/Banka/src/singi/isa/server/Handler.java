package singi.isa.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Handler implements Runnable{
	private Socket socket;
	private Racun racun;
	
	public Handler(Socket socket, Racun racun) {
		this.socket = socket;
		this.racun = racun;
	}
	
	@Override
	public void run() {
		try {
		PrintWriter os = new PrintWriter(socket.getOutputStream());
		BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		String input = is.readLine();
		while (input.equals("STOP")) {
		
		// moramo da isparsiramo zbog racuna
			double iznos = Double.parseDouble(input);
			
			racun.setRaspolozivo(racun.getRaspolozivo()+iznos);
			os.println("Transakcija je uspesna!");
			
			os.flush();
			input = is.readLine();
		}
		// zatvaraje konekcije
		os.close();
		is.close();
		socket.close();
		System.out.println(racun.getRaspolozivo());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	
}
