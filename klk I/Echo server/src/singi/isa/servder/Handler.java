package singi.isa.servder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Handler implements Runnable{
	private Socket socket;
	
	public Handler(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void run() {
		try {
		PrintWriter os = new PrintWriter(socket.getOutputStream());
		BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		System.out.println("Sadrzaj zahteva: "+ is.readLine());
		os.println("Odgvor");
		Thread.sleep(10000);
		os.flush();
		
		// zatvaraje konekcije
		os.close();
		is.close();
		socket.close();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	
}
