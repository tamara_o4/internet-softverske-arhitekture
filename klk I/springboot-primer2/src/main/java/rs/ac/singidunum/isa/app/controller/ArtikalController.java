package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.dto.ArtikalDTO;
import rs.ac.singidunum.isa.app.dto.KorisnikDTO;
import rs.ac.singidunum.isa.app.dto.KupovinaDTO;
import rs.ac.singidunum.isa.app.model.Artikal;
import rs.ac.singidunum.isa.app.model.Kupovina;
import rs.ac.singidunum.isa.app.service.ArtikalService;

// vrsi obradu, obradi zahtev i vrati rezultat
// http zahtevi get post put delete

@Controller
@RequestMapping(path = "/api/artikli")
public class ArtikalController {
	@Autowired
	private ArtikalService artikalService; 								
	
	// moramo mapirati metodu
	// PRIKAZ SVIH ARTIKALA
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<ArtikalDTO>> getAllArtikli(@RequestParam(name = "min", required = false) Double min, @RequestParam(name = "max", required = false) Double max) {
		//potrebano zbog pretrage cene izmeduju
		if (min == null) {
			min = -Double.MIN_VALUE;
		}
		if (max == null) {
			max = Double.MAX_VALUE;
		}
	
		
		ArrayList<ArtikalDTO> artikli = new ArrayList<ArtikalDTO>();
		for(Artikal artikal : artikalService.findByCenaBetween(min, max)) {
			ArrayList<KupovinaDTO> kupovine = new ArrayList<KupovinaDTO>();
			for(Kupovina kupovina : artikal.getKupovine()) {
				kupovine.add(new KupovinaDTO(kupovina.getId(), null, new KorisnikDTO(kupovina.getKorisnik().getId(), kupovina.getKorisnik().getKorisnickoIme(), null), kupovina.getDatumKupovine()));
			}
			artikli.add(new ArtikalDTO(artikal.getId(), artikal.getNaziv(), artikal.getCena(), artikal.getOpis(), kupovine));
		}
		
		return new ResponseEntity<Iterable<ArtikalDTO>>(artikli, HttpStatus.OK);
//		return new ResponseEntity<List<Artikal>>(artikalService.findByPriceBetween(min, max), HttpStatus.OK);	// mora da se uveze da ne bi bio null pointer u Artikli service
	}
	
	// moramo mapirati metodu
	// PRIKAZ JEDNOG ARTIKLA
	@RequestMapping(path = "/{artikalId}", method = RequestMethod.GET)
	public ResponseEntity<Artikal> getArtikal(@PathVariable("artikalId") Long artikalId) {
		//provera
		Optional<Artikal> artikal = artikalService.findOne(artikalId);
		if(artikal.isPresent()) {
			return new ResponseEntity<Artikal>(artikal.get(), HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);	// mora da se uveze da ne bi bio null pointer u Artikli service
	}
	
	// DODAVANJE
	@RequestMapping(path= "", method = RequestMethod.POST)
	public ResponseEntity<Artikal> createArtikal(@RequestBody Artikal artikal){
		try {
			artikal.setId(null);
			artikalService.save(artikal);
			return new ResponseEntity<Artikal>(artikal, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Artikal>(HttpStatus.BAD_REQUEST);
	}
	
	// IZMENA
	@RequestMapping(path = "/{artikalId}", method = RequestMethod.PUT)
	public ResponseEntity<Artikal> updateArtikal(@PathVariable("artikalId") Long arikalId, @RequestBody Artikal izmenjenArtikal) {
		Artikal artikal = artikalService.findOne(arikalId).orElse(null);
		if(artikal != null) {
			izmenjenArtikal.setId(arikalId);
			izmenjenArtikal = artikalService.save(izmenjenArtikal);
			return new ResponseEntity<Artikal>(izmenjenArtikal, HttpStatus.OK);  // vraca ono sto je zapisano u bazi
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
		
	}
	
	// BRISANJE
	@RequestMapping(path = "/{artikalId}", method = RequestMethod.DELETE)
	public ResponseEntity<Artikal> deleteArtikal(@PathVariable("artikalId") Long artikalId) {
		if(artikalService.findOne(artikalId).isPresent()) {
			artikalService.delete(artikalId);
			return new ResponseEntity<Artikal>(HttpStatus.OK);
		}
		return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
	}
	
	// 
	
	// metoda za pronalazenje artikala izmedju nekih iznosa implementirana je u findAll 
		
	
}
