package rs.ac.singidunum.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Korisnik;
import rs.ac.singidunum.isa.app.repository.KorisnikRepository;

@Service
public class KorisnikSerivice {
	@Autowired
	private KorisnikRepository korisnikRepositoty;
	
	public KorisnikSerivice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KorisnikSerivice(KorisnikRepository korisnikRepositoty) {
		super();
		this.korisnikRepositoty = korisnikRepositoty;
	}

	public KorisnikRepository getKorisnikRepository() {
		return korisnikRepositoty;
	}

	public void setKorisnikRepository(KorisnikRepository korisnikRepositoty) {
		this.korisnikRepositoty = korisnikRepositoty;
	}
	
	public Iterable<Korisnik> findAll(){
		return korisnikRepositoty.findAll();
	}
	
	public Optional<Korisnik> findOne(Long id) {
		return korisnikRepositoty.findById(id);
	}
	
	public Korisnik save(Korisnik korisnik){
		return korisnikRepositoty.save(korisnik);
	}
		
	public void delete(Long id) {
		 korisnikRepositoty.deleteById(id);
	}
	
	public void delete(Korisnik korisnik) {
		 korisnikRepositoty.delete(korisnik);
	}
	public Optional<Korisnik> findByKorisnickoIme(String korisnickoIme) {
		return korisnikRepositoty.findByKorisnickoIme(korisnickoIme);
	}
	
//	public List<Korisnik> findByStanicaId(Long id){
//		return korisnikRepositoty.findByStanicaId(id);
//	}
//	
	


}
