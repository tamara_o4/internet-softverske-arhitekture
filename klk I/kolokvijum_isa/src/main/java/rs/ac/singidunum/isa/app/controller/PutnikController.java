package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.PutnikDTO;
import rs.ac.singidunum.isa.app.model.Putnik;
import rs.ac.singidunum.isa.app.service.PutnikSerivice;

@Controller
@RequestMapping(path = "/api/putnici")
public class PutnikController {
	@Autowired
	private PutnikSerivice putnikService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<PutnikDTO>> getAllArtikli() {
		ArrayList<PutnikDTO> artikli = new ArrayList<PutnikDTO>();
		for (Putnik putnik : putnikService.findAll()) {
//			ArrayList<KupovinaDTO> kupovine = new ArrayList<KupovinaDTO>();
//			for (Kupovina kupovina : Putnik.getKupovine()) {
//				kupovine.add(new KupovinaDTO(kupovina.getId(), null, new KorisnikDTO(kupovina.getKorisnik().getId(),
//						kupovina.getKorisnik().getKorisnickoIme(), null), kupovina.getDatumKupovine()));
//			}
			artikli.add(new PutnikDTO(putnik.getId(), putnik.getIme(), putnik.getPrezime() ));
		}

		return new ResponseEntity<Iterable<PutnikDTO>>(artikli, HttpStatus.OK);
	}
	
	
	// pretraga jednog
	@RequestMapping(path = "/{putnikId}", method = RequestMethod.GET)
	public ResponseEntity<PutnikDTO> getKarta(@PathVariable("putnikId") Long putnikId) {
		Optional<Putnik> artikal = putnikService.findOne(putnikId);
		PutnikDTO karta;
		if (artikal.isPresent()) {
			karta = new PutnikDTO(artikal.get().getId(), artikal.get().getIme(), artikal.get().getPrezime());
			return new ResponseEntity<PutnikDTO>(karta, HttpStatus.OK);
		}
		return new ResponseEntity<PutnikDTO>(HttpStatus.NOT_FOUND);
	
	}
	
	
	// dodavanje
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Putnik> createPutnik(@RequestBody Putnik putnik) {
		try {
			putnikService.save(putnik);
			System.out.println("LOG");
			return new ResponseEntity<Putnik>(putnik, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Putnik>(HttpStatus.BAD_REQUEST);
	}

	// izmena
	@RequestMapping(path = "/{putnikId}", method = RequestMethod.PUT)
	public ResponseEntity<Putnik> updatePutnik(@PathVariable("putnikId") Long putnikId,
			@RequestBody Putnik izmenjeniPutnik) {
		Putnik putnik = putnikService.findOne(putnikId).orElse(null);
		if (putnik != null) {
			izmenjeniPutnik.setId(putnikId);
			izmenjeniPutnik = putnikService.save(izmenjeniPutnik);
			return new ResponseEntity<Putnik>(izmenjeniPutnik, HttpStatus.OK);
		}
		return new ResponseEntity<Putnik>(HttpStatus.NOT_FOUND);
	}

	
	// brisanje
	@RequestMapping(path = "/{putnikId}", method = RequestMethod.DELETE)
	public ResponseEntity<Putnik> deletePutnik(@PathVariable("putnikId") Long putnikId) {
		if (putnikService.findOne(putnikId).isPresent()) {
			putnikService.delete(putnikId);
			return new ResponseEntity<Putnik>(HttpStatus.OK);
		}
		return new ResponseEntity<Putnik>(HttpStatus.NOT_FOUND);
	}
	
}
