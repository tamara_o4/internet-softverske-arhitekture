package rs.ac.singidunum.isa.app.dto;


public class FakultetDTO {
	private Long id;
	private String naziv;
	private UniverzitetDTO univerzitet;
	private AdresaDTO adresal;
	
	public FakultetDTO() {
		super();
	}

	public FakultetDTO(Long id, String naziv, UniverzitetDTO univerzitet) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.univerzitet = univerzitet;
	}

	
	
	public FakultetDTO(Long id, String naziv, UniverzitetDTO univerzitet, AdresaDTO adresal) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.univerzitet = univerzitet;
		this.adresal = adresal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public UniverzitetDTO getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(UniverzitetDTO univerzitet) {
		this.univerzitet = univerzitet;
	}

	public AdresaDTO getAdresal() {
		return adresal;
	}

	public void setAdresal(AdresaDTO adresal) {
		this.adresal = adresal;
	}
	
	
}
