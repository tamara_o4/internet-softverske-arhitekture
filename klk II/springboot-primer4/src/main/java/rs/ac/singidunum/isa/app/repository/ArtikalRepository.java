package rs.ac.singidunum.isa.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Artikal;

@Repository
public interface ArtikalRepository extends CrudRepository<Artikal, Long> {
	List<Artikal> findByCenaBetween(double min, double max);
	
	@Query("SELECT a FROM Artikal a WHERE a.cena > :min AND a.cena < :max")
	List<Artikal> pronadjiPoCeni(double min, double max);
}
