package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Projekcija;
import rs.ac.singidunum.isa.app.repository.ProjekcijaRepository;

@Service
public class ProjekcijaSerivice {
	@Autowired
	private ProjekcijaRepository projekcijaRepository;

	public ProjekcijaSerivice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProjekcijaSerivice(ProjekcijaRepository projekcijaRepository) {
		super();
		this.projekcijaRepository = projekcijaRepository;
	}

	public ProjekcijaRepository getProjekcijaRepository() {
		return projekcijaRepository;
	}

	public void setProjekcijaRepository(ProjekcijaRepository projekcijaRepository) {
		this.projekcijaRepository = projekcijaRepository;
	}
	
	public Iterable<Projekcija> findAll(){
		return projekcijaRepository.findAll();
	}
	
	public Optional<Projekcija> findOne(Long id) {
		return projekcijaRepository.findById(id);
	}
	
	//
//	public List<Artikal> findByPriceBetween(double min, double max){
//		return artikalRepository.findByPriceBetween(min, max);
//	}
	
	public Projekcija save(Projekcija projekcija){
		return projekcijaRepository.save(projekcija);
	}
		
	public void delete(Long id) {
		 projekcijaRepository.deleteById(id);
	}
	
	public void delete(Projekcija projekcija) {
		 projekcijaRepository.delete(projekcija);
	}
	
	
}
