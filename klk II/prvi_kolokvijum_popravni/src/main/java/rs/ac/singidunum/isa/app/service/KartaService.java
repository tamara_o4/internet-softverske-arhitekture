package rs.ac.singidunum.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.repository.KartaRepository;

@Service
public class KartaService {
	@Autowired
	private KartaRepository kartaRepository;

	public KartaService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KartaService(KartaRepository kartaRepository) {
		super();
		this.kartaRepository = kartaRepository;
	}

	public KartaRepository getKartaRepository() {
		return kartaRepository;
	}

	public void setKartaRepository(KartaRepository kartaRepository) {
		this.kartaRepository = kartaRepository;
	}
	
	public Iterable<Karta> findAll(){
		return kartaRepository.findAll();
	}
	
	public Optional<Karta> findOne(Long id) {
		return kartaRepository.findById(id);
	}
	
	public Karta save(Karta korisnik){
		return kartaRepository.save(korisnik);
	}
		
	public void delete(Long id) {
		 kartaRepository.deleteById(id);
	}
	
	public void delete(Karta korisnik) {
		 kartaRepository.delete(korisnik);
	}
	
	public List<Karta> findByCenaGreaterThan(double min){
		return kartaRepository.findByCenaGreaterThan(min);
	}
	
}
