@Entity
@Repository

========================================================================================================================
@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
@Lob
	private String ime;
	
@Temporal(TemporalType.TIMESTAMP)
	private Date datumIVremePolaska;
	
=========================================================================================================================
@ManyToOne(optional = false)
	private FinansijskaKartica finansijskaKartica;
	
@OneToMany(mappedBy = "finansijskaKartica")
	private Set<Transakcija> karte = new HashSet<Transakcija>();
	
@OneToOne(mappedBy = "student")
	private FinansijskaKartica finansijskaKartica;
	
@JoinColumn(name = "user_id")
    private User user;
	
========================================================================================================================
	
@Repository
public interface PutnikRepository extends CrudRepository<Putnik, Long> {
}
	
	
========================================================================================================================

@Service

	@Autowired
	StanicaRepository stanicaRepository;
	
	public Iterable<Stanica> findAll() {
		return stanicaRepository.findAll();
	}
	
	public Optional<Stanica> findOne(Long id) {
		return stanicaRepository.findById(id);
	}
	
	public Stanica save(Stanica stanica) {
		return stanicaRepository.save(stanica);
	}
	
	public void delete(Long id) {
		stanicaRepository.deleteById(id);
	}
	
	public void delete(Stanica stanica) {
		stanicaRepository.delete(stanica);
	}

========================================================================================================================
	
@Controller
@RequestMapping(path = "/api/stanice")

private Service service;

######GET ALL######

@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<StudentDTO>> getAll(){
		ArrayList<StudentDTO> studenti = new ArrayList<StudentDTO>();
		for(Student student : studentService.findAll()) {
			studenti.add(new StudentDTO());
			
		}
		return new ResponseEntity<Iterable<StudentDTO>>(studenti, HttpStatus.OK);
	}
	
######GET ONE######

######POST######

######PUT######

######DELETE######

	
	
	