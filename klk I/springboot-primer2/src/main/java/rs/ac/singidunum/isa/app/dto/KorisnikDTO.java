package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import rs.ac.singidunum.isa.app.model.Kupovina;

public class KorisnikDTO {
	private Long id;
	
	private String korisnickoIme;
	private String lozinka;

	private Set<Kupovina> kupovine = new HashSet<Kupovina>();
	
	
	public KorisnikDTO(Long id, String korisnickoIme, String lozinka, Set<Kupovina> kupovine) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.kupovine = kupovine;
	}

	public KorisnikDTO(Long id, String korisnickoIme, String lozinka) {
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		new ArrayList<Kupovina>();
	}
	
	public KorisnikDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public Set<Kupovina> getKupovine() {
		return kupovine;
	}

	public void setKupovine(Set<Kupovina> kupovine) {
		this.kupovine = kupovine;
	}


	
	
}
