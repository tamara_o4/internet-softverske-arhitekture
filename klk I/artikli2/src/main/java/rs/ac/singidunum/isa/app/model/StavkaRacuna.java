package rs.ac.singidunum.isa.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class StavkaRacuna {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	private double kolicina;
	private double cena;
	
	@ManyToOne(optional = false)
	private Racun racun;
	@ManyToOne(optional = false)
	private ArtikalUMaloprodaji artikalUMaloprodaji;
	
	public StavkaRacuna() {
		super();
		// TODO Auto-generated constructor stub
	}
	public StavkaRacuna(Long id, double kolicina, double cena, Racun racun, ArtikalUMaloprodaji artikalUMaloprodaji) {
		super();
		this.id = id;
		this.kolicina = kolicina;
		this.cena = cena;
		this.racun = racun;
		this.artikalUMaloprodaji = artikalUMaloprodaji;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getKolicina() {
		return kolicina;
	}
	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public Racun getRacun() {
		return racun;
	}
	public void setRacun(Racun racun) {
		this.racun = racun;
	}
	public ArtikalUMaloprodaji getArtikalUMaloprodaji() {
		return artikalUMaloprodaji;
	}
	public void setArtikalUMaloprodaji(ArtikalUMaloprodaji artikalUMaloprodaji) {
		this.artikalUMaloprodaji = artikalUMaloprodaji;
	}
	
	
	
	
}
