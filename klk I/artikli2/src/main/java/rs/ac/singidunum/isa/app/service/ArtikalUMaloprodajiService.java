package rs.ac.singidunum.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.ArtikalUMaloprodaji;
import rs.ac.singidunum.isa.app.repository.ArtikalUMaloprodajiRepository;

@Service
public class ArtikalUMaloprodajiService {
	@Autowired
	private ArtikalUMaloprodajiRepository artikalUMaloprodajiRepository;

	public ArtikalUMaloprodajiService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ArtikalUMaloprodajiService(ArtikalUMaloprodajiRepository artikalUMaloprodajiRepository) {
		super();
		this.artikalUMaloprodajiRepository = artikalUMaloprodajiRepository;
	}

	public ArtikalUMaloprodajiRepository getArtikalUMaloprodajiRepository() {
		return artikalUMaloprodajiRepository;
	}

	public void setArtikalUMaloprodajiRepository(ArtikalUMaloprodajiRepository artikalUMaloprodajiRepository) {
		this.artikalUMaloprodajiRepository = artikalUMaloprodajiRepository;
	}
	
	public Iterable<ArtikalUMaloprodaji> findAll(){
		return artikalUMaloprodajiRepository.findAll();
	}
	
	public Optional<ArtikalUMaloprodaji> findOne(Long id) {
		return artikalUMaloprodajiRepository.findById(id);
	}
	
	public ArtikalUMaloprodaji save(ArtikalUMaloprodaji artikal){
		return artikalUMaloprodajiRepository.save(artikal);
	}
		
	public void delete(Long id) {
		artikalUMaloprodajiRepository.deleteById(id);
	}
	
	public void delete(ArtikalUMaloprodaji artikal) {
		artikalUMaloprodajiRepository.delete(artikal);
	}
	
	public List<ArtikalUMaloprodaji> findByPopust(Long id){
		return artikalUMaloprodajiRepository.findByPopust(id);
	}
}
