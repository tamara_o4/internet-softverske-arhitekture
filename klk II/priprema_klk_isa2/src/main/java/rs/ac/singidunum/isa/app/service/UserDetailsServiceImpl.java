package rs.ac.singidunum.isa.app.service;

import java.util.ArrayList;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Student;
import rs.ac.singidunum.isa.app.model.UserPermission;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	StudentService studentService;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// Dobavljanje studenta po korisnickom imenu.
		Optional<Student> student = studentService.findByIndeks(username);
		
		if(student.isPresent()) {
			// Formiranje liste dodeljenih prava pristupa.
			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			for(UserPermission userPermission : student.get().getUserPermissions()) {
				grantedAuthorities.add(new SimpleGrantedAuthority(userPermission.getPermission().getTitle()));
			}
			
			// Kreiranje studenta na osnovu korisnickog imena, lozinke i dodeljenih prava pristupa.
			return new org.springframework.security.core.userdetails.User(student.get().getIndeks(), student.get().getLozinka(), grantedAuthorities);
		}
		return null;
	}
}
