package rs.ac.singidunum.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.model.Kupac;
import rs.ac.singidunum.isa.app.repository.KartaRepository;
import rs.ac.singidunum.isa.app.repository.KupacRepository;

@Service
public class KupacSerivice {
	@Autowired
	private KupacRepository kupacRepository;
	
	@Autowired
	private KartaRepository kartaRepository;

	public KupacSerivice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KupacSerivice(KupacRepository kupacRepository) {
		super();
		this.kupacRepository = kupacRepository;
	}

	public KupacRepository getKupacRepository() {
		return kupacRepository;
	}

	public void setKupacRepository(KupacRepository kupacRepository) {
		this.kupacRepository = kupacRepository;
	}
	
	public Iterable<Kupac> findAll(){
		return kupacRepository.findAll();
	}
	
	public Optional<Kupac> findOne(Long id) {
		return kupacRepository.findById(id);
	}
	
	public Kupac save(Kupac kupac){
		return kupacRepository.save(kupac);
	}
		
	public void delete(Long id) {
		 kupacRepository.deleteById(id);
	}
	
	public void delete(Kupac kupac) {
		 kupacRepository.delete(kupac);
	}
	
//	public List<Kupac> findByStanicaId(Long id){
//		return kupacRepository.findByStanicaId(id);
//	}
//	
//	
	public List<Karta> findByProjekcijaId(Long id){
	return kartaRepository.findByProjekcijaId(id);
	}

}
