package rs.ac.singidunum.isa.app.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Racun {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datumIzdavanja;
	
	@OneToMany(mappedBy = "racun")
	private Set<StavkaRacuna> stavkeRacuna = new HashSet<StavkaRacuna>();

	public Racun() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Racun(Long id, Date datumIzdavanja, Set<StavkaRacuna> stavkeRacuna) {
		super();
		this.id = id;
		this.datumIzdavanja = datumIzdavanja;
		this.stavkeRacuna = stavkeRacuna;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDatumIzdavanja() {
		return datumIzdavanja;
	}

	public void setDatumIzdavanja(Date datumIzdavanja) {
		this.datumIzdavanja = datumIzdavanja;
	}

	public Set<StavkaRacuna> getStavkeRacuna() {
		return stavkeRacuna;
	}

	public void setStavkeRacuna(Set<StavkaRacuna> stavkeRacuna) {
		this.stavkeRacuna = stavkeRacuna;
	}
	
	
	
}
