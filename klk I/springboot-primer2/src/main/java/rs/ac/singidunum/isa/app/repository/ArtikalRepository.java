package rs.ac.singidunum.isa.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Artikal;

@Repository
public interface ArtikalRepository extends CrudRepository<Artikal, Long>{
	
	
	List<Artikal> findByCenaBetween(double min, double max);  
	/* 
	 * find - radi se select
	 * ByCena - atribut po kome se radi
	 * Between - kako se radi selekcija
	 */

//	@Query("SELECT objkat FROM Artikal objekat WHERE cena >= :min and cena < :max") //moze i between
	List<Artikal> pronadjiPoCeni(double min, double max);
	// ovu metodu pozivamo u servisu
}
