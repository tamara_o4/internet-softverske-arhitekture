package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.dto.ArtikalUMaloprodajiDTO;
import rs.ac.singidunum.isa.app.dto.PopustDTO;
import rs.ac.singidunum.isa.app.model.ArtikalUMaloprodaji;
import rs.ac.singidunum.isa.app.service.ArtikalUMaloprodajiService;

@Controller
@RequestMapping(path = "/api/artikli-u-maloprodaji")
public class AtrikalUMaloprodajiController {
	@Autowired
	private ArtikalUMaloprodajiService artikalUMaloprodajiService;

	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<ArtikalUMaloprodajiDTO>> getAllArtikli(@RequestParam(name = "id", required = false) Long id) {
		//potrebano zbog pretrage cene izmeduju
		if (id == null) {
			id = (long) 1; // samo za test, ne znam sta bih dugo ovde stavila
		}
		ArrayList<ArtikalUMaloprodajiDTO> artikliUMaloprodaji = new ArrayList<ArtikalUMaloprodajiDTO>();
		for (ArtikalUMaloprodaji artikalUMaloprodaji : artikalUMaloprodajiService.findAll()){
			artikliUMaloprodaji.add(new ArtikalUMaloprodajiDTO(artikalUMaloprodaji.getId(), artikalUMaloprodaji.getCena(),
					artikalUMaloprodaji.getKolicina(), new PopustDTO(artikalUMaloprodaji.getPopust().getId(), 
					artikalUMaloprodaji.getPopust().getNaziv(), artikalUMaloprodaji.getPopust().getPocetak(), 
					artikalUMaloprodaji.getPopust().getKraj(), artikalUMaloprodaji.getPopust().getProcenat())));
			
		}

		return new ResponseEntity<Iterable<ArtikalUMaloprodajiDTO>>(artikliUMaloprodaji, HttpStatus.OK);
	}


	
}
