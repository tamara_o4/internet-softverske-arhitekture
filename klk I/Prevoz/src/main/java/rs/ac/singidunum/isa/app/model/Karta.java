package rs.ac.singidunum.isa.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Karta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datumVremePolaska;
	private boolean prtljag;
	private Double cena;
	
	// veze
	@ManyToOne(optional = false)
	private Putnik putnik;
	@ManyToOne(optional = false)
	private Stanica stanica;
	
	public Karta() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Karta(Long id, Date datumVremePolaska, boolean prtljag, Double cena, Putnik putnik, Stanica stanica) {
		super();
		this.id = id;
		this.datumVremePolaska = datumVremePolaska;
		this.prtljag = prtljag;
		this.cena = cena;
		this.putnik = putnik;
		this.stanica = stanica;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDatumVremePolaska() {
		return datumVremePolaska;
	}
	public void setDatumVremePolaska(Date datumVremePolaska) {
		this.datumVremePolaska = datumVremePolaska;
	}
	public boolean isPrtljag() {
		return prtljag;
	}
	public void setPrtljag(boolean prtljag) {
		this.prtljag = prtljag;
	}
	public Double getCena() {
		return cena;
	}
	public void setCena(Double cena) {
		this.cena = cena;
	}
	public Putnik getPutnik() {
		return putnik;
	}
	public void setPutnik(Putnik putnik) {
		this.putnik = putnik;
	}
	public Stanica getStanica() {
		return stanica;
	}
	public void setStanica(Stanica stanica) {
		this.stanica = stanica;
	}
	
	
}
