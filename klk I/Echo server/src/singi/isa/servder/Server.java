package singi.isa.servder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	
	public static void main(String[] args) throws InterruptedException {
		int port = 3000;
		
		try (ServerSocket ss = new ServerSocket(port)){
			while(true) {
				
			
			Socket socket = ss.accept(); 				// uspostavljena veza, OVDE SE CITAJU ILI PISU PODACI
		
			PrintWriter os = new PrintWriter(socket.getOutputStream());
			BufferedReader is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		
			System.out.println("Sadrzaj zahteva: "+ is.readLine());
			os.println("Odgvor");
			os.flush();
			
			// zatvaraje konekcije
			os.close();
			is.close();
			socket.close();
			//Thread.sleep(10000);
			}
		} catch (IOException e) {
			e.printStackTrace();
			System.out.print("Doslo je do greske pri pokretanju servera!");
		}  	
		
	}
}
	