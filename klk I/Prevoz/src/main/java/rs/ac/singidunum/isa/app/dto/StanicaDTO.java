package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;


public class StanicaDTO {
	private Long id;	
	private String naziv;
	private String adresa;
	
	private ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();

	public StanicaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StanicaDTO(Long id, String naziv, String adresa, ArrayList<KartaDTO> karte) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.adresa = adresa;
		this.karte = karte;
	}

	public StanicaDTO(Long id, String naziv, String adresa) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.adresa = adresa;
		new ArrayList<KartaDTO>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public ArrayList<KartaDTO> getKarte() {
		return karte;
	}

	public void setKarte(ArrayList<KartaDTO> karte) {
		this.karte = karte;
	}
	
	
	
}
