package rs.ac.singidunum.isa.app.model;


import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;


@Entity
public class Nastavnik {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;					
	@Lob
	private String ime;
	@Lob
	//@Column(columnDefinition = "TINYTEX", insertable = true)
	private String biografija;
	@Lob
	private String jmbg;
	
	@OneToMany(mappedBy = "nastavnik")
	private Set<Zvanje> zvanja = new HashSet<Zvanje>();

	public Nastavnik(Long id, String ime, String biografija, String jmbg) {
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
	}

	public Nastavnik(Long id, String ime, String biografija, String jmbg, Set<Zvanje> zvanja) {
		super();
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
		this.zvanja = zvanja;
	}

	public Nastavnik() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getBiografija() {
		return biografija;
	}

	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}

	public Set<Zvanje> getZvanja() {
		return zvanja;
	}

	public void setZvanja(Set<Zvanje> zvanja) {
		this.zvanja = zvanja;
	}
	
	// moramo dodati zvog veze sa Kupovinom
//	@OneToMany(mappedBy = "artikal")
//	private Set<Kupovina> kupovine = new HashSet<Kupovina>();

	
	
}
