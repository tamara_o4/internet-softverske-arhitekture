package rs.ac.singidunum.isa.app.dto;

public class AdresaDTO {
	private Long id;
	private String ulica;
	private String broj;
	private MestoDTO mesto;
	private UniverzitetDTO univerzitet;
	private FakultetDTO fakultet;
	
	public AdresaDTO() {
		super();
	}

	public AdresaDTO(Long id, String ulica, String broj, MestoDTO mesto, UniverzitetDTO univerzitet) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
		this.mesto = mesto;
		this.univerzitet = univerzitet;
	}

	
	
	public AdresaDTO(Long id, String ulica, String broj, MestoDTO mesto, UniverzitetDTO univerzitet,
			FakultetDTO fakultet) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.broj = broj;
		this.mesto = mesto;
		this.univerzitet = univerzitet;
		this.fakultet = fakultet;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getBroj() {
		return broj;
	}

	public void setBroj(String broj) {
		this.broj = broj;
	}

	public MestoDTO getMesto() {
		return mesto;
	}

	public void setMesto(MestoDTO mesto) {
		this.mesto = mesto;
	}

	public UniverzitetDTO getUniverzitet() {
		return univerzitet;
	}

	public void setUniverzitet(UniverzitetDTO univerzitet) {
		this.univerzitet = univerzitet;
	}

	public FakultetDTO getFakultet() {
		return fakultet;
	}

	public void setFakultet(FakultetDTO fakultet) {
		this.fakultet = fakultet;
	}
	
	
}
