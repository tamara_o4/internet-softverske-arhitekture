package rs.ac.singidunum.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.repository.KartaRepository;

@Service
public class KartaSerivice {
	@Autowired
	private KartaRepository kartaRepository;

	public KartaSerivice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KartaSerivice(KartaRepository kartaRepository) {
		super();
		this.kartaRepository = kartaRepository;
	}

	public KartaRepository getKartaRepository() {
		return kartaRepository;
	}

	public void setKartaRepository(KartaRepository kartaRepository) {
		this.kartaRepository = kartaRepository;
	}
	
	public Iterable<Karta> findAll(){
		return kartaRepository.findAll();
	}
	
	public Optional<Karta> findOne(Long id) {
		return kartaRepository.findById(id);
	}

	
	public List<Karta> findByPutnik(Long id) {
		return (kartaRepository).findByPutnik(id);
	}
	
	public Karta save(Karta Karta){
		return kartaRepository.save(Karta);
	}
		
	public void delete(Long id) {
		kartaRepository.deleteById(id);
	}
	
	public void delete(Karta Karta) {
		kartaRepository.delete(Karta);
	}
	
	
}
