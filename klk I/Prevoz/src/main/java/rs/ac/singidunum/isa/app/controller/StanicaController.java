package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.StanicaDTO;
import rs.ac.singidunum.isa.app.dto.KartaDTO;
import rs.ac.singidunum.isa.app.dto.PutnikDTO;
import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.model.Stanica;
import rs.ac.singidunum.isa.app.service.StanicaSerivice;

@Controller
@RequestMapping(path = "/api/stanice")
public class StanicaController {
	@Autowired
	private StanicaSerivice stanicaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<StanicaDTO>> getAllStanice(){
		ArrayList<StanicaDTO> stanice = new ArrayList<StanicaDTO>();
		for(Stanica stanica : stanicaService.findAll()) {
			ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
			for (Karta karta : stanica.getKarte()) {
				PutnikDTO putnik = new PutnikDTO(karta.getPutnik().getId(), karta.getPutnik().getIme(), karta.getPutnik().getPrezime());
				karte.add(new KartaDTO(karta.getId(), karta.getDatumVremePolaska(), karta.isPrtljag(), karta.getCena(), putnik, null));
			}
			stanice.add(new StanicaDTO(stanica.getId(), stanica.getNaziv(), stanica.getAdresa(), karte));
		
		}
	return new ResponseEntity<Iterable<StanicaDTO>>(stanice, HttpStatus.OK);
	}
	
	// pretraga po idju
	@RequestMapping(path = "/{stanicaId}", method = RequestMethod.GET)
	public ResponseEntity<StanicaDTO> getStanica(@PathVariable("stanicaId") Long stanicaId) {
		Optional<Stanica> stanica = stanicaService.findOne(stanicaId);
		StanicaDTO stanicaDto;
		if (stanica.isPresent()) {
			//kartaDTO = new KartaDTO(karta.get().getId(), karta.get().getDatumVremePolaska(), karta.get().isPrtljag(), karta.get().getCena(), putnik, stanica);
			ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
			for (Karta karta : stanica.get().getKarte()) {
				PutnikDTO putnik = new PutnikDTO(karta.getPutnik().getId(), karta.getPutnik().getIme(), karta.getPutnik().getPrezime());
				karte.add(new KartaDTO(karta.getId(), karta.getDatumVremePolaska(), karta.isPrtljag(), karta.getCena(), putnik, null));
			}
			stanicaDto = new StanicaDTO(stanica.get().getId(), stanica.get().getNaziv(), stanica.get().getAdresa(), karte);
			return new ResponseEntity<StanicaDTO>(stanicaDto, HttpStatus.OK);
		}
		return new ResponseEntity<StanicaDTO>(HttpStatus.NOT_FOUND);
	}
	
	// novi
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Stanica> createStanica(@RequestBody Stanica stanica) {
		try {
			stanica.setId(null);
			stanicaService.save(stanica);
			return new ResponseEntity<Stanica>(stanica, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Stanica>(HttpStatus.BAD_REQUEST);
	}
	
	// izmena
	@RequestMapping(path = "/{stanicaId}", method = RequestMethod.PUT)
	public ResponseEntity<StanicaDTO> updateStanica(@PathVariable("stanicaId") Long stanicaId,
			@RequestBody Stanica izmenjeni) {
		Stanica stanica = stanicaService.findOne(stanicaId).orElse(null);
		if (stanica != null) {
			izmenjeni.setId(stanicaId);
			izmenjeni = stanicaService.save(izmenjeni);
			ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
			for (Karta karta : stanica.getKarte()) {
				PutnikDTO putnik = new PutnikDTO(karta.getPutnik().getId(), karta.getPutnik().getIme(), karta.getPutnik().getPrezime());
				karte.add(new KartaDTO(karta.getId(), karta.getDatumVremePolaska(), karta.isPrtljag(), karta.getCena(), putnik, null));
			}
			StanicaDTO izmenjenDto = new StanicaDTO(stanica.getId(), stanica.getNaziv(), stanica.getAdresa(), karte);
			return new ResponseEntity<StanicaDTO>(izmenjenDto, HttpStatus.OK);
		}
		return new ResponseEntity<StanicaDTO>(HttpStatus.NOT_FOUND);
	}
	
	// brisanje
	@RequestMapping(path = "/{stanicaId}", method = RequestMethod.DELETE)
	public ResponseEntity<Stanica> deleteKarta(@PathVariable("stanicaId") Long stanicaId) {
		if (stanicaService.findOne(stanicaId).isPresent()) {
			stanicaService.delete(stanicaId);
			return new ResponseEntity<Stanica>(HttpStatus.OK);
		}
		return new ResponseEntity<Stanica>(HttpStatus.NOT_FOUND);
	}
	
}
