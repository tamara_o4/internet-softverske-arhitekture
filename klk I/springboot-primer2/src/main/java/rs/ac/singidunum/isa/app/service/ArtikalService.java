package rs.ac.singidunum.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Artikal;
import rs.ac.singidunum.isa.app.repository.ArtikalRepository;

@Service
public class ArtikalService {
	@Autowired
	private ArtikalRepository artikalRepository;
	

	public ArtikalService() {
		super();
	}

	public ArtikalRepository getArtikalRepository() {
		return artikalRepository;
	}

	public void setArtikalRepository(ArtikalRepository artikalRepository) {
		this.artikalRepository = artikalRepository;
	}

	public ArtikalService(ArtikalRepository artikalRepository) {
		super();
		this.artikalRepository = artikalRepository;
	}
	
	public Iterable<Artikal> findAll(){
		return artikalRepository.findAll();
	}
	
	public Optional<Artikal> findOne(Long id) {
		return artikalRepository.findById(id);
	}
	
	
	//
	public List<Artikal> findByCenaBetween(double min, double max){
		return artikalRepository.findByCenaBetween(min, max);
	}
	
	public Artikal save(Artikal artikal){
		return artikalRepository.save(artikal);
	}
		
	public void delete(Long id) {
		 artikalRepository.deleteById(id);
	}
	
	public void delete(Artikal artikal) {
		 artikalRepository.delete(artikal);
	}
	
	public boolean postaviPopust(Long id, double popust) {
		Optional<Artikal> artikal= artikalRepository.findById(id);
		if(artikal.isPresent()) {
			artikal.get().setCena(artikal.get().getCena() - artikal.get().getCena()*popust);
			//ne mora da znaci da ce biti zapisano
			artikalRepository.save(artikal.get());
			return true;
		}
		return false;
	}
}
