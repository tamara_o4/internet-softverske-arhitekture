package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

@Entity
public class Putnik {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String ime;
	@Lob
	private String prezime;
	
	@OneToMany(mappedBy = "putnik")
	private Set<Karta> karte = new HashSet<Karta>();

	public Putnik() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Putnik(Long id, String ime, String prezime, Set<Karta> karte ) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.karte = karte;
	}

	public Putnik(Long id, String ime, String prezime) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		new HashSet<Karta>();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public Set<Karta> getkarte() {
		return karte;
	}

	public void setkarte(Set<Karta> karte) {
		this.karte = karte;
	}
	
	
	
}
