package rs.ac.singidunum.isa.app.controller;

import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.dto.StudentDTO;
import rs.ac.singidunum.isa.app.dto.TokenDTO;
import rs.ac.singidunum.isa.app.model.Student;
import rs.ac.singidunum.isa.app.model.UserPermission;
import rs.ac.singidunum.isa.app.service.StudentService;
import rs.ac.singidunum.isa.app.service.PermissionService;
import rs.ac.singidunum.isa.app.utils.TokenUtils;

@Controller
@RequestMapping("/api")
public class LoginController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private TokenUtils tokenUtils;

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private StudentService studentService;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public ResponseEntity<TokenDTO> login(@RequestBody StudentDTO korisnik) {
		try {
			// Kreiranje tokena za login, token sadrzi korisnicko ime i lozinku.
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
					korisnik.getIndeks(), korisnik.getLozinka());
			// Autentifikacija korisnika na osnovu korisnickog imena i lozinke.
			Authentication authentication = authenticationManager.authenticate(token);
			// Dodavanje uspesne autentifikacije u security context.
			SecurityContextHolder.getContext().setAuthentication(authentication);

			// Ucitavanje podatka o korisniku i kreiranje jwt-a.
			UserDetails userDetails = userDetailsService.loadUserByUsername(korisnik.getIndeks());
			String jwt = tokenUtils.generateToken(userDetails);
			TokenDTO jwtDTO = new TokenDTO(jwt);

			return new ResponseEntity<TokenDTO>(jwtDTO, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<TokenDTO>(HttpStatus.UNAUTHORIZED);
		}
	}

	@RequestMapping(path = "/register", method = RequestMethod.POST)
	public ResponseEntity<StudentDTO> register(@RequestBody StudentDTO korisnik) {
		// Novi korisnik se registruje kreiranjem instance korisnika
		// cija je lozinka enkodovana.
		Student noviKorisnik = new Student(null, korisnik.getIndeks(),
				passwordEncoder.encode(korisnik.getLozinka()));
		noviKorisnik = studentService.save(noviKorisnik);
		// Dodavanje prava pristupa.
		noviKorisnik.setUserPermissions(new HashSet<UserPermission>());
		noviKorisnik.getUserPermissions()
				.add(new UserPermission(null, noviKorisnik, permissionService.findOne(1l).get()));
		studentService.save(noviKorisnik);

		return new ResponseEntity<StudentDTO>(
				new StudentDTO(noviKorisnik.getId(), noviKorisnik.getIndeks(), null), HttpStatus.OK);
	}
}
