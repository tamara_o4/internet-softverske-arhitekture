package rs.ac.singidunum.isa.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//prvo moramo da naznacimo da ova klasa predstavalja spring boot app

@SpringBootApplication
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);	// args - argumenti komandne linije
	}
}
