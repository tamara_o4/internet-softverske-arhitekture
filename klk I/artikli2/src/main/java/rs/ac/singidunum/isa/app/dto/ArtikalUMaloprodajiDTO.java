package rs.ac.singidunum.isa.app.dto;

import java.util.HashSet;
import java.util.Set;

public class ArtikalUMaloprodajiDTO {
	private Long id;					// Long zbo identity, objekat je zato sto moze biti nul
	private double cena;
	private double kolicina;
	private PopustDTO popust;
	private Set<StavkaRacunaDTO> stavkeRacuna = new HashSet<StavkaRacunaDTO>();
	
	public ArtikalUMaloprodajiDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ArtikalUMaloprodajiDTO(Long id, double cena, double kolicina, PopustDTO popust,
			Set<StavkaRacunaDTO> stavkeRacuna) {
		super();
		this.id = id;
		this.cena = cena;
		this.kolicina = kolicina;
		this.popust = popust;
		this.stavkeRacuna = stavkeRacuna;
	}
	
	public ArtikalUMaloprodajiDTO(Long id, double cena, double kolicina, PopustDTO popust) {
		super();
		this.id = id;
		this.cena = cena;
		this.kolicina = kolicina;
		this.popust = popust;
		new HashSet<StavkaRacunaDTO>();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public double getKolicina() {
		return kolicina;
	}
	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}
	public PopustDTO getPopust() {
		return popust;
	}
	public void setPopust(PopustDTO popust) {
		this.popust = popust;
	}
	public Set<StavkaRacunaDTO> getStavkeRacuna() {
		return stavkeRacuna;
	}
	public void setStavkeRacuna(Set<StavkaRacunaDTO> stavkeRacuna) {
		this.stavkeRacuna = stavkeRacuna;
	}
	
	

}
