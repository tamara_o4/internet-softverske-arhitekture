package rs.ac.singidunum.isa.app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class NaucniRad {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String naziv;
	
	// autori mogu biti drugi nastavnici ili osobe koje nisu nastavnici registrovani  sistemu
	// neka veza sa korisnikom
	
	// kljucne reci
	
	// apstrakt
	
	// kategorijaa

}
