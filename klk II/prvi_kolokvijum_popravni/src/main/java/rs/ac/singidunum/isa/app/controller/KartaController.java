package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.dto.KartaDTO;
import rs.ac.singidunum.isa.app.dto.KupacDTO;
import rs.ac.singidunum.isa.app.dto.ProjekcijaDTO;
import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.service.KartaService;

@Controller
@RequestMapping(path = "/api/karte")
public class KartaController {
	@Autowired
	private KartaService kartaService;
	// pronalazenje sa ocenom vecom od zadate


	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<KartaDTO>> getAllKarte(@RequestParam(name = "min", required = false) Double min){
		ArrayList<KartaDTO> karte = new ArrayList<KartaDTO>();
		if (min == null) {
			//min = -Double.MIN_VALUE;
			for(Karta karta : kartaService.findAll()) {
//				
				KupacDTO k = new KupacDTO(karta.getKupac().getId(), karta.getKupac().getIme(), karta.getKupac().getPrezime());
				ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena());
				karte.add(new KartaDTO(karta.getId(), karta.getPocetakPrijave(), karta.getBrojSedista(), karta.getCena(), k, p));
			}
		}
		else {
			for(Karta karta : kartaService.findByCenaGreaterThan(min)) {
				KupacDTO k = new KupacDTO(karta.getKupac().getId(), karta.getKupac().getIme(), karta.getKupac().getPrezime());
				ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena());
				karte.add(new KartaDTO(karta.getId(), karta.getPocetakPrijave(), karta.getBrojSedista(), karta.getCena(), k, p));
				}
		}
		return new ResponseEntity<Iterable<KartaDTO>>(karte, HttpStatus.OK);
	}
	
	
	// pretraga po idju
	@RequestMapping(path = "/{kartaId}", method = RequestMethod.GET)
	public ResponseEntity<KartaDTO> getKarta(@PathVariable("kartaId") Long kartaId) {
		Optional<Karta> karta = kartaService.findOne(kartaId);
		KartaDTO kartaDTO;
		if (karta.isPresent()) {
			KupacDTO k = new KupacDTO(karta.get().getKupac().getId(), karta.get().getKupac().getIme(), karta.get().getKupac().getPrezime());
			ProjekcijaDTO p = new ProjekcijaDTO(karta.get().getProjekcija().getId(), karta.get().getProjekcija().getFilm(), karta.get().getProjekcija().getZanr(), karta.get().getProjekcija().getOcena());
			kartaDTO = new KartaDTO(karta.get().getId(), karta.get().getPocetakPrijave(), karta.get().getBrojSedista(), karta.get().getCena(), k, p);
			
			return new ResponseEntity<KartaDTO>(kartaDTO, HttpStatus.OK);
		}
		return new ResponseEntity<KartaDTO>(HttpStatus.NOT_FOUND);
	}
	
	// novi
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Karta> createKarta(@RequestBody Karta karta) {
		try {
			karta.setId(null);
			kartaService.save(karta);
			return new ResponseEntity<Karta>(karta, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Karta>(HttpStatus.BAD_REQUEST);
	}
	
	// izmena
	@RequestMapping(path = "/{kartaId}", method = RequestMethod.PUT)
	public ResponseEntity<KartaDTO> updateKarta(@PathVariable("kartaId") Long kartaId,
			@RequestBody Karta izmenjeniKarta) {
		Karta karta = kartaService.findOne(kartaId).orElse(null);
		if (karta != null) {
			izmenjeniKarta.setId(kartaId);
			izmenjeniKarta = kartaService.save(izmenjeniKarta);
			KupacDTO k = new KupacDTO(karta.getKupac().getId(), karta.getKupac().getIme(), karta.getKupac().getPrezime());
			ProjekcijaDTO p = new ProjekcijaDTO(karta.getProjekcija().getId(), karta.getProjekcija().getFilm(), karta.getProjekcija().getZanr(), karta.getProjekcija().getOcena());
			KartaDTO izmenjenaKartaDto = new KartaDTO(izmenjeniKarta.getId(), izmenjeniKarta.getPocetakPrijave(), izmenjeniKarta.getBrojSedista(),izmenjeniKarta.getCena(), k, p); 
			return new ResponseEntity<KartaDTO>(izmenjenaKartaDto, HttpStatus.OK);
		}
		return new ResponseEntity<KartaDTO>(HttpStatus.NOT_FOUND);
	}
	
	// brisanje
	@RequestMapping(path = "/{kartaId}", method = RequestMethod.DELETE)
	public ResponseEntity<Karta> deleteKarta(@PathVariable("kartaId") Long kartaId) {
		if (kartaService.findOne(kartaId).isPresent()) {
			kartaService.delete(kartaId);
			return new ResponseEntity<Karta>(HttpStatus.OK);
		}
		return new ResponseEntity<Karta>(HttpStatus.NOT_FOUND);
	}

}
