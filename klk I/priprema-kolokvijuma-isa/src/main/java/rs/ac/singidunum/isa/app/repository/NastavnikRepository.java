package rs.ac.singidunum.isa.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Nastavnik;

@Repository
public interface NastavnikRepository extends CrudRepository<Nastavnik, Long>{


}
