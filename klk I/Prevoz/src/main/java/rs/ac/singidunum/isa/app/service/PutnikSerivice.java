package rs.ac.singidunum.isa.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.model.Putnik;
import rs.ac.singidunum.isa.app.repository.KartaRepository;
import rs.ac.singidunum.isa.app.repository.PutnikRepository;

@Service
public class PutnikSerivice {
	@Autowired
	private PutnikRepository putnikRepository;
	
	@Autowired
	private KartaRepository kartaRepository;

	public PutnikSerivice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PutnikSerivice(PutnikRepository putnikRepository) {
		super();
		this.putnikRepository = putnikRepository;
	}

	public PutnikRepository getPutnikRepository() {
		return putnikRepository;
	}

	public void setPutnikRepository(PutnikRepository putnikRepository) {
		this.putnikRepository = putnikRepository;
	}
	
	public Iterable<Putnik> findAll(){
		return putnikRepository.findAll();
	}
	
	public Optional<Putnik> findOne(Long id) {
		return putnikRepository.findById(id);
	}
	
	public Putnik save(Putnik korisnik){
		return putnikRepository.save(korisnik);
	}
		
	public void delete(Long id) {
		 putnikRepository.deleteById(id);
	}
	
	public void delete(Putnik korisnik) {
		 putnikRepository.delete(korisnik);
	}
	
//	public List<Putnik> findByStanicaId(Long id){
//		return putnikRepository.findByStanicaId(id);
//	}
//	
	
	public List<Karta> findByStanicaId(Long id){
	return kartaRepository.findByStanicaId(id);
}

}
