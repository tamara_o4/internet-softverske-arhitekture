package rs.ac.singidunum.isa.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.model.Kupovina;
import rs.ac.singidunum.isa.app.service.KupovinaService;

@Controller
@RequestMapping(path = "/api/kupovine")
public class KupovinaController {
	@Autowired
	private KupovinaService kupovinaService; 	
	
	// PRIKAZ SVIH
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<Kupovina>> getAllKupovine() {
		return new ResponseEntity<Iterable<Kupovina>>(kupovinaService.findAll(), HttpStatus.OK);
	}
		
}
