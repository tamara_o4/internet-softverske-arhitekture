package rs.ac.singidunum.isa.app.dto;

import java.util.Date;

public class KartaDTO {
	private Long id;
	
	private Date pocetakPrijave;
	private String brojSedista;
	private Double cena;
	private KupacDTO kupac;
	private ProjekcijaDTO projekcija;
	
	public KartaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public KartaDTO(Long id, Date pocetakPrijave, String brojSedista, Double cena, KupacDTO kupac,
			ProjekcijaDTO projekcija) {
		super();
		this.id = id;
		this.pocetakPrijave = pocetakPrijave;
		this.brojSedista = brojSedista;
		this.cena = cena;
		this.kupac = kupac;
		this.projekcija = projekcija;
	}
	
	public KartaDTO(Long id, Date pocetakPrijave, String brojSedista, Double cena) {
		super();
		this.id = id;
		this.pocetakPrijave = pocetakPrijave;
		this.brojSedista = brojSedista;
		this.cena = cena;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getPocetakPrijave() {
		return pocetakPrijave;
	}
	public void setPocetakPrijave(Date pocetakPrijave) {
		this.pocetakPrijave = pocetakPrijave;
	}
	public String getBrojSedista() {
		return brojSedista;
	}
	public void setBrojSedista(String brojSedista) {
		this.brojSedista = brojSedista;
	}
	public Double getCena() {
		return cena;
	}
	public void setCena(Double cena) {
		this.cena = cena;
	}
	public KupacDTO getKupac() {
		return kupac;
	}
	public void setKupac(KupacDTO kupac) {
		this.kupac = kupac;
	}
	public ProjekcijaDTO getProjekcija() {
		return projekcija;
	}
	public void setProjekcija(ProjekcijaDTO projekcija) {
		this.projekcija = projekcija;
	}
	
	
}
