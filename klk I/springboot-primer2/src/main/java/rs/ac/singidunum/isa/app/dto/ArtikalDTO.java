package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

public class ArtikalDTO {
	private Long id;					
	private String naziv;
	private double cena;
	private String opis;
	private ArrayList<KupovinaDTO> kupovina = new ArrayList<KupovinaDTO>();
	
	public ArtikalDTO() {
		super();
	}
	public ArtikalDTO(Long id, String naziv, double cena, String opis) {
		this.id = id;
		this.naziv = naziv;
		this.cena = cena;
		this.opis = opis;
		new ArrayList<KupovinaDTO>();
	}
	public ArtikalDTO(Long id, String naziv, double cena, String opis, ArrayList<KupovinaDTO> kupovina) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.cena = cena;
		this.opis = opis;
		this.kupovina = kupovina;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public ArrayList<KupovinaDTO> getKupovina() {
		return kupovina;
	}
	public void setKupovina(ArrayList<KupovinaDTO> kupovina) {
		this.kupovina = kupovina;
	}
	
	
	
}
