package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Student;
import rs.ac.singidunum.isa.app.repository.StudentRepository;

@Service
public class StudentService {
	@Autowired
	private StudentRepository studentRepository;
	
	public Iterable<Student> findAll(){
		return studentRepository.findAll();
	}
	
	public Optional<Student> findOne(Long id) {
		return studentRepository.findById(id);
	}
	
	public Student save(Student korisnik){
		return studentRepository.save(korisnik);
	}
		
	public void delete(Long id) {
		 studentRepository.deleteById(id);
	}
	
	public void delete(Student korisnik) {
		 studentRepository.delete(korisnik);
	}

	public Optional<Student> findByIndeks(String username) {
		return studentRepository.findByIndeks(username);
	}
}
