package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;

import rs.ac.singidunum.isa.app.model.Student;

public class FinansijskaKarticaDTO {
	private Long id;
	private String pozivNaBroj;
	private Double stanje;
	
	private Student student;
	
	private ArrayList<TransakcijaDTO> transakcije = new ArrayList<TransakcijaDTO>();

	public FinansijskaKarticaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FinansijskaKarticaDTO(Long id, String pozivNaBroj, Double stanje, Student student,
			ArrayList<TransakcijaDTO> transakcije) {
		super();
		this.id = id;
		this.pozivNaBroj = pozivNaBroj;
		this.stanje = stanje;
		this.student = student;
		this.transakcije = transakcije;
	}


	public FinansijskaKarticaDTO(Long id, String pozivNaBroj, Double stanje, Student student) {
		super();
		this.id = id;
		this.pozivNaBroj = pozivNaBroj;
		this.stanje = stanje;
		this.student = student;
		new ArrayList<TransakcijaDTO>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPozivNaBroj() {
		return pozivNaBroj;
	}

	public void setPozivNaBroj(String pozivNaBroj) {
		this.pozivNaBroj = pozivNaBroj;
	}

	public Double getStanje() {
		return stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public ArrayList<TransakcijaDTO> getTransakcije() {
		return transakcije;
	}

	public void setTransakcije(ArrayList<TransakcijaDTO> transakcije) {
		this.transakcije = transakcije;
	}
	
	

}
