package rs.ac.singidunum.isa.app.dto;

import java.util.Date;


public class KartaDTO {
	private Long id;
	private Date datumVremePolaska;
	
	private boolean prtljag;
	private double cena;
	private PutnikDTO putnik;
	private StanicaDTO stanica;

	public KartaDTO(Long id, Date datumVremePolaska, boolean prtljag, double cena, PutnikDTO putnik,
			StanicaDTO stanica) {
		super();
		this.id = id;
		this.datumVremePolaska = datumVremePolaska;
		this.prtljag = prtljag;
		this.cena = cena;
		this.putnik = putnik;
		this.stanica = stanica;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDatumVremePolaska() {
		return datumVremePolaska;
	}
	public void setDatumVremePolaska(Date datumVremePolaska) {
		this.datumVremePolaska = datumVremePolaska;
	}
	public boolean isPrtljag() {
		return prtljag;
	}
	public void setPrtljag(boolean prtljag) {
		this.prtljag = prtljag;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public PutnikDTO getPutnik() {
		return putnik;
	}
	public void setPutnik(PutnikDTO putnik) {
		this.putnik = putnik;
	}
	public StanicaDTO getStanica() {
		return stanica;
	}
	public void setStanica(StanicaDTO stanica) {
		this.stanica = stanica;
	}
	
	
}
