package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ac.singidunum.isa.app.model.Racun;
import rs.ac.singidunum.isa.app.repository.RacunRepository;

@Service
public class RacunService {
	@Autowired
	private RacunRepository racunRepository;

	public RacunService() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RacunService(RacunRepository racunRepository) {
		super();
		this.racunRepository = racunRepository;
	}

	public RacunRepository getRacunRepository() {
		return racunRepository;
	}

	public void setRacunRepository(RacunRepository racunRepository) {
		this.racunRepository = racunRepository;
	}
	
	public Iterable<Racun> findAll(){
		return racunRepository.findAll();
	}
	
	public Optional<Racun> findOne(Long id) {
		return racunRepository.findById(id);
	}
	
	public Racun save(Racun artikal){
		return racunRepository.save(artikal);
	}
		
	public void delete(Long id) {
		racunRepository.deleteById(id);
	}
	
	public void delete(Racun artikal) {
		racunRepository.delete(artikal);
	}
}
