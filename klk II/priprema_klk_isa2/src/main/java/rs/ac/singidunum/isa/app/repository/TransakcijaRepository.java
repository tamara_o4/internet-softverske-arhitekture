package rs.ac.singidunum.isa.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import rs.ac.singidunum.isa.app.model.Transakcija;

@Repository
public interface TransakcijaRepository extends CrudRepository<Transakcija, Long> {
	
}
