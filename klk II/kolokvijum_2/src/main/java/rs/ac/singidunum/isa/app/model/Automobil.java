package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.Id;
@Entity
public class Automobil {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String naziv;
	private double cena;
	private long kolicina;
	
	@OneToMany(mappedBy = "automobil")
	private Set<Registracija> registracije = new HashSet<Registracija>();

	public Automobil() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Automobil(Long id, String naziv, double cena, long kolicina, Set<Registracija> registracije) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.cena = cena;
		this.kolicina = kolicina;
		this.registracije = registracije;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public long getKolicina() {
		return kolicina;
	}

	public void setKolicina(long kolicina) {
		this.kolicina = kolicina;
	}

	public Set<Registracija> getKarte() {
		return registracije;
	}

	public void setKarte(Set<Registracija> registracije) {
		this.registracije = registracije;
	}
	
	
}
