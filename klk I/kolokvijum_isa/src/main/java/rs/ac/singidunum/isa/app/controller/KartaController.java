package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import rs.ac.singidunum.isa.app.dto.KartaDTO;
import rs.ac.singidunum.isa.app.dto.PutnikDTO;
import rs.ac.singidunum.isa.app.model.Karta;
import rs.ac.singidunum.isa.app.service.KartaSerivice;

@Controller
@RequestMapping(path = "/api/karte")
public class KartaController {
	@Autowired
	private KartaSerivice kartaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Iterable<KartaDTO>> getAllArtikli(@RequestParam(name = "id", required = false) Long id) {
		if (id == null) {
			id = (long) 1; // samo za test, ne znam sta bih dugo ovde stavila
		}

		ArrayList<KartaDTO> artikli = new ArrayList<KartaDTO>();
		
		//for (Karta karta : kartaService.findByPutnik(id)) {
		for (Karta karta : kartaService.findAll()) {
			artikli.add(new KartaDTO(karta.getId(), karta.getDatumVremePolaska(), karta.isPrtljag(), karta.getCena(), new PutnikDTO(
					karta.getPutnik().getId(), karta.getPutnik().getIme(), karta.getPutnik().getPrezime()), null));
		}

		return new ResponseEntity<Iterable<KartaDTO>>(artikli, HttpStatus.OK);
	}
	
	
	// pretraga jednog
	@RequestMapping(path = "/{kartaId}", method = RequestMethod.GET)
	public ResponseEntity<KartaDTO> getKarta(@PathVariable("kartaId") Long kartaId) {
		Optional<Karta> artikal = kartaService.findOne(kartaId);
		KartaDTO karta;
		if (artikal.isPresent()) {
			karta = new KartaDTO(artikal.get().getId(), artikal.get().getDatumVremePolaska(), artikal.get().isPrtljag(), artikal.get().getCena(), null, null);
			return new ResponseEntity<KartaDTO>(karta, HttpStatus.OK);
		}
		return new ResponseEntity<KartaDTO>(HttpStatus.NOT_FOUND);
	}
	
	// dodavanje
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Karta> createKarta(@RequestBody Karta karta) {
		try {
			kartaService.save(karta);
			System.out.println("LOG");
			return new ResponseEntity<Karta>(karta, HttpStatus.CREATED);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<Karta>(HttpStatus.BAD_REQUEST);
	}

	// izmena
	@RequestMapping(path = "/{kartaId}", method = RequestMethod.PUT)
	public ResponseEntity<Karta> updateKarta(@PathVariable("kartaId") Long kartaId,
			@RequestBody Karta izmenjeniKarta) {
		Karta karta = kartaService.findOne(kartaId).orElse(null);
		if (karta != null) {
			izmenjeniKarta.setId(kartaId);
			izmenjeniKarta = kartaService.save(izmenjeniKarta);
			return new ResponseEntity<Karta>(izmenjeniKarta, HttpStatus.OK);
		}
		return new ResponseEntity<Karta>(HttpStatus.NOT_FOUND);
	}

	
	// brisanje
	@RequestMapping(path = "/{kartaId}", method = RequestMethod.DELETE)
	public ResponseEntity<Karta> deleteKarta(@PathVariable("kartaId") Long kartaId) {
		if (kartaService.findOne(kartaId).isPresent()) {
			kartaService.delete(kartaId);
			return new ResponseEntity<Karta>(HttpStatus.OK);
		}
		return new ResponseEntity<Karta>(HttpStatus.NOT_FOUND);
	}
}
