package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class KorisnikDTO {
	private Long id;

	private String korisnickoIme;
	private String lozinka;
	private String ime;
	private String prezime;
	private Set<RegistracijaDTO> registracije = new HashSet<RegistracijaDTO>();
	public KorisnikDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public KorisnikDTO(Long id, String korisnickoIme, String lozinka, String ime, String prezime,
			Set<RegistracijaDTO> registracije) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.ime = ime;
		this.prezime = prezime;
		this.registracije = registracije;
	}
	
	public KorisnikDTO(Long id, String korisnickoIme, String lozinka) {
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		new ArrayList<RegistracijaDTO>();
	}
	
	public KorisnikDTO(Long id, String korisnickoIme, String lozinka, String ime, String prezime) {
		super();
		this.id = id;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.ime = ime;
		this.prezime = prezime;
		new ArrayList<RegistracijaDTO>();
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public Set<RegistracijaDTO> getRegistracije() {
		return registracije;
	}
	public void setRegistracije(Set<RegistracijaDTO> registracije) {
		this.registracije = registracije;
	}
	
	
}
