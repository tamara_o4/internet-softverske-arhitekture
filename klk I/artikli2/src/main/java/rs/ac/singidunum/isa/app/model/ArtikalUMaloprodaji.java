package rs.ac.singidunum.isa.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class ArtikalUMaloprodaji {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;					// Long zbo identity, objekat je zato sto moze biti null
	
	private double cena;
	private double kolicina;

	@ManyToOne(optional = false)
	private Popust popust;
	
	@OneToMany(mappedBy = "artikalUMaloprodaji")
	private Set<StavkaRacuna> stavkeRacuna = new HashSet<StavkaRacuna>();

	public ArtikalUMaloprodaji() {
		super();
		// TODO Auto-generated constructor stub
	}



	public ArtikalUMaloprodaji(Long id, double cena, double kolicina, Popust popust, Set<StavkaRacuna> stavkeRacuna) {
		super();
		this.id = id;
		this.cena = cena;
		this.kolicina = kolicina;
		this.popust = popust;
		this.stavkeRacuna = stavkeRacuna;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public double getKolicina() {
		return kolicina;
	}

	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}

	public Popust getPopust() {
		return popust;
	}

	public void setPopust(Popust popust) {
		this.popust = popust;
	}

	public Set<StavkaRacuna> getStavkeRacuna() {
		return stavkeRacuna;
	}

	public void setStavkeRacuna(Set<StavkaRacuna> stavkeRacuna) {
		this.stavkeRacuna = stavkeRacuna;
	}
	
	
	
}
