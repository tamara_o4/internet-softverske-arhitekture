package rs.ac.singidunum.isa.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import rs.ac.singidunum.isa.app.model.UserPermission;
import rs.ac.singidunum.isa.app.repository.UserPermissionRepository;

public class UserPermissionService {
	@Autowired
	private UserPermissionRepository userPermissionRepository;
	
	public Iterable<UserPermission> findAll() {
		return userPermissionRepository.findAll();
	}
	
	public Optional<UserPermission> findOne(Long id) {
		return userPermissionRepository.findById(id);
	}
	
	public UserPermission save(UserPermission userPermission) {
		return userPermissionRepository.save(userPermission);
	}
	
	public void delete(Long id) {
		userPermissionRepository.deleteById(id);
	}
	
	public void delete(UserPermission userPermission) {
		userPermissionRepository.delete(userPermission);
	}
}
