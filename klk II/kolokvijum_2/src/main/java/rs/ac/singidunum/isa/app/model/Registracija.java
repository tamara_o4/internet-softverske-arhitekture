package rs.ac.singidunum.isa.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Id;

@Entity
public class Registracija {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Lob
	private String registracioniBroj;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date datumRegistracija;
	
	@Lob
	private String status;
	
	@ManyToOne(optional = false)
	private Korisnik korisnik;
	
	@ManyToOne(optional = false)
	private Automobil automobil;

	public Registracija() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Registracija(Long id, String registracioniBroj, Date datumRegistracija, String status, Korisnik korisnik,
			Automobil automobil) {
		super();
		this.id = id;
		this.registracioniBroj = registracioniBroj;
		this.datumRegistracija = datumRegistracija;
		this.status = status;
		this.korisnik = korisnik;
		this.automobil = automobil;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegistracioniBroj() {
		return registracioniBroj;
	}

	public void setRegistracioniBroj(String registracioniBroj) {
		this.registracioniBroj = registracioniBroj;
	}

	public Date getDatumRegistracija() {
		return datumRegistracija;
	}

	public void setDatumRegistracija(Date datumRegistracija) {
		this.datumRegistracija = datumRegistracija;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Automobil getAutomobil() {
		return automobil;
	}

	public void setAutomobil(Automobil automobil) {
		this.automobil = automobil;
	}
	
	

}
