package rs.ac.singidunum.isa.app.dto;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


import rs.ac.singidunum.isa.app.model.StavkaRacuna;

public class RacunDTO {
	
	private Long id;	
	private Date datumIzdavanja;
	private Set<StavkaRacunaDTO> stavkeRacuna = new HashSet<StavkaRacunaDTO>();
	
	
	public RacunDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public RacunDTO(Long id, Date datumIzdavanja, Set<StavkaRacunaDTO> stavkeRacuna) {
		super();
		this.id = id;
		this.datumIzdavanja = datumIzdavanja;
		this.stavkeRacuna = stavkeRacuna;
	}
	
	public RacunDTO(Long id, Date datumIzdavanja) {
		super();
		this.id = id;
		this.datumIzdavanja = datumIzdavanja;
		new HashSet<StavkaRacuna>();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDatumIzdavanja() {
		return datumIzdavanja;
	}
	public void setDatumIzdavanja(Date datumIzdavanja) {
		this.datumIzdavanja = datumIzdavanja;
	}
	public Set<StavkaRacunaDTO> getStavkeRacuna() {
		return stavkeRacuna;
	}
	public void setStavkeRacuna(Set<StavkaRacunaDTO> stavkeRacuna) {
		this.stavkeRacuna = stavkeRacuna;
	}
	
	

}
