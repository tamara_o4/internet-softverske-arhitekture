package rs.ac.singidunum.isa.app.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.ac.singidunum.isa.app.aspect.Logged;
import rs.ac.singidunum.isa.app.dto.FinansijskaKarticaDTO;
import rs.ac.singidunum.isa.app.model.FinansijskaKartica;
import rs.ac.singidunum.isa.app.service.FinansijskaKarticaService;

@Controller
@RequestMapping(path = "/api/studenti")
public class FinansijskaKarticaController {
	@Autowired
	private FinansijskaKarticaService finansijskaKarticaService;
	
	@Logged
	@RequestMapping(path = "", method = RequestMethod.GET)
	@Secured("ROLE_ADMIN")
	public ResponseEntity<Iterable<FinansijskaKarticaDTO>> getAllStudenti(){
		ArrayList<FinansijskaKarticaDTO> studenti = new ArrayList<FinansijskaKarticaDTO>();
		for(FinansijskaKartica s : finansijskaKarticaService.findAll()) {
//			PutnikDTO putnik = new PutnikDTO(karta.getPutnik().getId(), karta.getPutnik().getIme(), karta.getPutnik().getPrezime());
//			StanicaDTO stanica = new StanicaDTO(karta.getStanica().getId(), karta.getStanica().getNaziv(), karta.getStanica().getAdresa());
			//studenti.add(new StudentDTO(s.getId(), s.getIndeks(), s.getIme(), s.getPrezime(), s.getEmail(), s.getLozinka(), null));
			studenti.add(new FinansijskaKarticaDTO(s.getId(), s.getPozivNaBroj(), s.getStanje(), null));
		}
		
		return new ResponseEntity<Iterable<FinansijskaKarticaDTO>>(studenti, HttpStatus.OK);
	}

}
