package rs.ac.singidunum.isa.app.dto;

import java.util.ArrayList;


import rs.ac.singidunum.isa.app.model.Zvanje;

public class NastavniklDTO {
	private Long id;					
	private String ime;
	//@Column(columnDefinition = "TINYTEX", insertable = true)
	private String biografija;
	private String jmbg;
	private ArrayList<ZvanjeDTO> zvanja = new ArrayList<ZvanjeDTO>();
	public NastavniklDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public NastavniklDTO(Long id, String ime, String biografija, String jmbg, ArrayList<ZvanjeDTO> zvanja) {
		super();
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
		this.zvanja = zvanja;
	}
	public NastavniklDTO(Long id, String ime, String biografija, String jmbg) {
		this.id = id;
		this.ime = ime;
		this.biografija = biografija;
		this.jmbg = jmbg;
		new ArrayList<ZvanjeDTO>();
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getBiografija() {
		return biografija;
	}
	public void setBiografija(String biografija) {
		this.biografija = biografija;
	}
	public String getJmbg() {
		return jmbg;
	}
	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}
	public ArrayList<ZvanjeDTO> getZvanja() {
		return zvanja;
	}
	public void setZvanja(ArrayList<ZvanjeDTO> zvanja) {
		this.zvanja = zvanja;
	}
	
}
